import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LotterySlider from '../../components/LotterySlider/LotterySlider';
import BorderBlock from '../../components/BorderBlock/BorderBlock';
import NewsBlock from '../../components/NewsBlock/NewsBlock';
import './Lottery.css';

const prizeTotalImg = require('../../assets/imgs/prize_banner_total.png');

export default class Lottery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: true,
      newsList: [],
    };
  }

  componentDidMount() {
    this.fetchNews();
  }

  fetchNews = () => {
    fetch('https://api.stoloto.ru/mobile/api/v20/service/news/sportloto')
      .then((response) => {
        if (response.status !== 200) {
          throw response.statusText;
        }
        return response;
      })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        this.setState({ newsList: json, isFetching: false });
      })
      .catch((err) => {
        this.setState({ isFetching: false });
        console.log(err);
      });
  };

  render() {
    const { newsList, isFetching } = this.state;
    return (
      <article className="page page--lottery">
        <LotterySlider />
        <div className="page__info-block container">
          <div
            style={{ display: 'none' }}
            className="page__prize-banner lottery-prize-banner"
          >
            <h4 className="lottery-prize-banner__header">
              Выплаты победителям составили более
            </h4>
            <div className="lottery-prize-banner__prize-block">
              <img
                className="lottery-prize-banner__prize-img"
                src={prizeTotalImg}
                alt="max prize in rubles"
              />
              <span className="lottery-prize-banner__prize-value">Рублей</span>
            </div>
          </div>
          <div className="page__news-tab">
            <BorderBlock
              hasLink
              isInnerLink
              hasPadding
              hasBottomBorder
              linkText="Новостная лента"
              linkUrl="/news"
              borderType="left"
            >
              <div className="news-tab">
                <h3 className="news-tab__header">Последние новости</h3>
                <div
                  className="news-tab__list"
                  style={{
                    transition: 'opacity .2s ease',
                    opacity: isFetching ? 0 : 1,
                  }}
                >
                  {newsList.slice(0, 3).map((i, index) => (
                    <div className="news-tab__item" key={index}>
                      <NewsBlock news={i} />
                    </div>
                  ))}
                </div>
              </div>
            </BorderBlock>
          </div>
        </div>
        <div className="page__info-block container">
          <div className="page__text-block">
            <h2 className="page__header">
              Всероссийские государственные тиражные лотереи &laquo;Спортлото
              Матчбол&raquo; и&nbsp;&laquo;КЕНО-Спортлото&raquo;. Всероссийские
              государственные бестиражные лотереи
            </h2>
            <p className="page__text">
              В&nbsp;пакет Всероссийских государственных лотерей
              &laquo;Спортлото&raquo; входят две тиражные лотереи&nbsp;&mdash;{' '}
              <strong>
                <a
                  className="page__link"
                  href="https://www.stoloto.ru/5x50?ad=sportloto_5x50"
                >
                  &laquo;Спортлото Матчбол&raquo;
                </a>
              </strong>{' '}
              и&nbsp;
              <strong>
                <a
                  className="page__link"
                  href="https://www.stoloto.ru/keno?ad=sportloto_keno"
                >
                  &laquo;КЕНО&nbsp;&mdash; Спортлото&raquo;
                </a>
              </strong>
              &nbsp;&mdash; и&nbsp;6&nbsp;видов{' '}
              <Link className="page__link" to="/ticket_types">
                бестиражных лотерей.
              </Link>
            </p>
            <p className="page__text">
              Билеты тиражных лотерей &laquo;Спортлото&raquo; можно купить как
              в&nbsp;режиме онлайн, так и&nbsp;в&nbsp;розничных пунктах продаж.
              Билеты бестиражных лотерей &laquo;Спортлото&raquo; можно купить
              в&nbsp;отделениях Почты России, магазинах &laquo;Связной&raquo;,
              пунктах продаж лотерей и&nbsp;лотерейном центре
              &laquo;Столото&raquo; (Волгоградский проспект, 43, к.&nbsp;3).
            </p>
            <p className="page__text">
              Тиражи{' '}
              <strong>
                <a
                  className="page__link"
                  href="https://www.stoloto.ru/5x50/rules?ad=sportloto_5x50"
                >
                  &laquo;Спортлото Матчбол&raquo;
                </a>
              </strong>{' '}
              проходят каждый день в&nbsp;20:00 по&nbsp;московскому времени.{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/5x50/game?ad=sportloto_5x50"
              >
                Купить билеты
              </a>,{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/5x50/check_bulletin?ad=sportloto_5x50"
              >
                проверить билеты
              </a>, а&nbsp;также{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/lottery/onlinesale?ad=sportloto_5x50"
              >
                получить выигрыши
              </a>{' '}
              на&nbsp;сумму до&nbsp;100&nbsp;000 рублей можно на&nbsp;сайте{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/?ad=sportloto_5x50"
              >
                stoloto.ru
              </a>.
            </p>
            <p className="page__text">
              Розыгрыши тиражей лотереи{' '}
              <strong>
                <a
                  className="page__link"
                  href="https://www.stoloto.ru/keno/full_rules?ad=sportloto_keno"
                >
                  &laquo;КЕНО-Спортлото&raquo;
                </a>{' '}
              </strong>
              проводятся каждые 15&nbsp;минут.{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/quickgames/keno?ad=sportloto_keno"
              >
                Купить билеты
              </a>,{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/keno/check_bulletin?ad=sportloto_keno"
              >
                проверить билеты
              </a>, а&nbsp;также{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/lottery/onlinesale?ad=sportloto_keno"
              >
                получить выигрыши
              </a>{' '}
              на&nbsp;сумму до&nbsp;100&nbsp;000 рублей можно на&nbsp;сайте{' '}
              <a
                className="page__link"
                href="https://www.stoloto.ru/?ad=sportloto_keno"
              >
                stoloto.ru
              </a>.
            </p>
            <p className="page__text">
              Получить информацию о&nbsp;лотереях
              (в&nbsp;т.ч.&nbsp;о&nbsp;правилах участия, результатах розыгрыша
              тиражей) можно на&nbsp;сайте{' '}
              <a
                className="page__link"
                href="https://www.lotonews.ru/?ad=sportloto"
              >
                www.lotonews.ru
              </a>{' '}
              и&nbsp;
              <a
                className="page__link"
                href="https://www.stoloto.ru/?ad=sportloto"
              >
                www.stoloto.ru
              </a>.
            </p>
            <p className="page__text">
              <a
                className="page__link"
                href="https://www.stoloto.ru/5x50/archive/?ad=sportloto_5x50"
              >
                Архив тиражей &laquo;Спортлото Матчбол&raquo;
              </a>
            </p>
            <p className="page__text">
              <a
                className="page__link"
                href="https://www.stoloto.ru/keno/archive/?ad=sportloto_keno"
              >
                Архив тиражей &laquo;КЕНО-Спортлото&raquo;
              </a>
            </p>
            <p className="page__text">
              <a
                className="page__link"
                href="https://www.stoloto.ru/6x49/archive/?ad=sportloto_6x49"
              >
                Архив тиражей &laquo;Спортлото 6&nbsp;из&nbsp;49&raquo;
              </a>
            </p>
          </div>
        </div>
      </article>
    );
  }
}
