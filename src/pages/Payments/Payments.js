import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import LotteryTab from '../../components/LotteryTab/LotteryTab';
import './Payments.css';

const prizeCurrencyImg = require('../../assets/imgs/prize_num_cur.png');

const numberImgZero = require('../../assets/imgs/prize_num_0.png');
const numberImgOne = require('../../assets/imgs/prize_num_1.png');
const numberImgTwo = require('../../assets/imgs/prize_num_2.png');
const numberImgThree = require('../../assets/imgs/prize_num_3.png');
const numberImgFour = require('../../assets/imgs/prize_num_4.png');
const numberImgFive = require('../../assets/imgs/prize_num_5.png');
const numberImgSix = require('../../assets/imgs/prize_num_6.png');
const numberImgSeven = require('../../assets/imgs/prize_num_7.png');
const numberImgEight = require('../../assets/imgs/prize_num_8.png');
const numberImgNine = require('../../assets/imgs/prize_num_9.png');

const numberImgs = [
  numberImgZero,
  numberImgOne,
  numberImgTwo,
  numberImgThree,
  numberImgFour,
  numberImgFive,
  numberImgSix,
  numberImgSeven,
  numberImgEight,
  numberImgNine,
];

const parseNumToImg = (number) =>
  number
    .toString()
    .split('')
    .map((i, index) => (
      <img
        className="page-prize-banner__number"
        key={index}
        src={numberImgs[i]}
        alt="prize digit"
      />
    ));

const Payments = () => (
  <article className="page page--payments">
    <Helmet>
      <title>Выплата выигрышей - Спортлото</title>
      <meta
        name="description"
        content="Информация о выплаченных выигрышах в лотереях Спортлото"
      />
      <meta
        name="keywords"
        content="Лотерея, Спортлото, Выиграть в лотерею, Партнер сочи 2014"
      />
    </Helmet>
    <div className="container">
      <h3 className="page__header">Выплачено выигрышей более</h3>
      <div className="page__prize-banner page-prize-banner">
        {parseNumToImg(1000000000)}
        <div>
          <img
            className="page-prize-banner__currency"
            src={prizeCurrencyImg}
            alt="ruble currency"
          />
        </div>
      </div>
      <div className="page__info-block">
        <div className="page__text">
          <p>
            <strong>
              Выплаты победителям лотерей, оператором которых является ООО
              &laquo;Спортлото&raquo;, составили более 1&nbsp;млрд рублей.
            </strong>
          </p>
          <p>
            <strong>
              По&nbsp;всей России продано свыше 230 млн билетов лотерей,
              оператором которых является ООО &laquo;Спортлото&raquo;.
            </strong>
          </p>
          <p>
            35&nbsp;миллионов участников стали{' '}
            <Link className="page__link" to="/winners">
              победителями
            </Link>, из&nbsp;них 73&nbsp;человека стали миллионерами.
          </p>
          <p>
            В&nbsp;настоящее время ООО &laquo;Спортлото&raquo; является лидером
            в&nbsp;сегменте моментальных лотерей российского лотерейного рынка.
            Каждую секунду продается 1&nbsp;билет{' '}
            <a
              className="page__link"
              href="https://www.sportloto.ru/ticket_types/"
            >
              моментальной лотереи
            </a>, каждый четвертый билет&nbsp;&mdash; выигрышный.
          </p>
          <p>
            Билеты моментальных (бестиражных) лотерей можно купить
            в&nbsp;отделениях Почты России, магазинах &laquo;Связной&raquo;,
            пунктах продаж лотерей и&nbsp;лотерейном центре
            &laquo;Столото&raquo; (Волгоградский проспект, 43, к.&nbsp;3).
          </p>
          <p>
            *Фраза &laquo;Каждую секунду продается 1&nbsp;билет моментальной
            лотереи&raquo; означает среднее значение, рассчитанное исходя
            из&nbsp;показателей об&nbsp;общем количестве проданных билетов
            за&nbsp;период
            с&nbsp;01.01.2012&nbsp;г.&nbsp;по&nbsp;31.12.2016&nbsp;г.
            (157&nbsp;680&nbsp;000&nbsp;секунд) всероссийских государственных
            бестиражных лотерей, проводимых на&nbsp;основании распоряжения
            Правительства РФ&nbsp;от&nbsp;14.09.2009&nbsp;г.
            &#8470;&nbsp;1318-р.
          </p>
          <p>
            **Фраза &laquo;Каждый четвертый билет&nbsp;&mdash; выигрышный&raquo;
            означает среднестатистическое значение, рассчитанное, исходя
            из&nbsp;показателей об&nbsp;общем количестве выигравших и&nbsp;общем
            количестве проданных лотерейных билетов всероссийских
            государственных бестиражных лотерей &laquo;Быстрее, выше,
            сильнее!&raquo;, &laquo;Вперед к&nbsp;победе&raquo;,
            &laquo;Поехали!&raquo;, &laquo;Вершины успеха&raquo;, &laquo;Спорт
            без границ&raquo;, &laquo;Спортивный сезон&raquo;, &laquo;Праздник
            спорта&raquo;, &laquo;Узоры на&nbsp;льду&raquo;, &laquo;Русские
            игры&raquo;, &laquo;Веселые старты&raquo; за&nbsp;период
            с&nbsp;01.01.2016&nbsp;г.&nbsp;по&nbsp;31.12.2016&nbsp;г.
          </p>
        </div>
        <div className="page__lotteries page__lotteries--vertical">
          <LotteryTab
            listDirection="vertical"
            headerText="Узнать больше о&nbsp;лотереях"
            hasLotteryLink
          />
        </div>
      </div>
    </div>
  </article>
);

export default Payments;
