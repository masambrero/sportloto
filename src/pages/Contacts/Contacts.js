import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import Map from '../../components/Map/Map';
import ArrowButton from '../../components/ArrowButton/ArrowButton';
import './Contacts.css';

export default class Contacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMapShown: true,
    };
  }

  toggleMap = () =>
    this.setState((prevState) => ({ isMapShown: !prevState.isMapShown }));

  render() {
    const { isMapShown } = this.state;

    return (
      <article className="page page--contacts">
        <Helmet>
          <title>Контакты</title>
          <meta name="description" content="Контактная информация Спортлото" />
          <meta name="keywords" content="Лотерея спортлото, контакты" />
        </Helmet>
        <div className="page__text-block container">
          <p className="page__text">
            Почтовый адрес ООО &laquo;Спортлото&raquo;:
          </p>
          <p className="page__text">
            109316, Москва, Волгоградский проспект, д.&nbsp;43, корп.&nbsp;3
          </p>
          <p className="page__text">
            Для получения выигрышей обращайтесь в&nbsp;лотерейный центр
            &laquo;Столото&raquo;
          </p>
          <p className="page__text">
            Москва<br />
            Волгоградский проспект, д.&nbsp;43,
            корп.&nbsp;3&nbsp;(М.&nbsp;Текстильщики)<br />
            Режим работы консультантов по&nbsp;выплатам:<br />
            ежедневно, с&nbsp;09:00 до&nbsp;23:00<br />
            без перерыва на&nbsp;обед и&nbsp;выходных<br />
            <a
              className="page__link"
              href="https://www.youtube.com/watch?time_continue=2&v=BDYY_QkQGcc"
            >
              Видеомаршрут
            </a>
          </p>
        </div>
        <div className="page__map map-container">
          <div className="map-container__controls">
            <div className="container">
              <ArrowButton
                activeState={isMapShown ? 'opened' : 'closed'}
                onClick={this.toggleMap}
              >
                {isMapShown ? 'Свернуть карту' : 'Развернуть карту'}
              </ArrowButton>
            </div>
          </div>
          <div
            className={`map-container__map-wrapper ${
              isMapShown
                ? 'map-container__map-wrapper--shown'
                : 'map-container__map-wrapper--hidden'
            }`}
          >
            <Map />
          </div>
        </div>
      </article>
    );
  }
}
