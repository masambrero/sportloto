import React from 'react';
import { Helmet } from 'react-helmet';
import './InfoDisclosure.css';

const InfoDisclosure = () => (
  <article className="page page--info-disclosure">
    <Helmet>
      <title>
        Правовая информация об официальном сайте Всероссийских Государственных
        лотерей Спортлото
      </title>
      <meta
        name="description"
        content="Официальный сайт лотерей Спортлото. Правовая информация для интернет-пользователей сайта Всероссийских Государственных тиражных и бестиражных лотерей Спортлото"
      />
      <meta name="keywords" content="сайт лотереи" />
    </Helmet>
    <div className="page__text-block container">
      <p className="page__text">
        <ul>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/zakluchenie_sportloto_2016.pdf"
            >
              Аудиторское заключение за&nbsp;2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-1_2016.pdf"
            >
              Отчет ВГБЛ-1 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-2_2016.pdf"
            >
              Отчет ВГБЛ-2 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-3_2016.pdf"
            >
              Отчет ВГБЛ-3 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-4_2016.pdf"
            >
              Отчет ВГБЛ-4 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-5_2016.pdf"
            >
              Отчет ВГБЛ-5 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-6_2016.pdf"
            >
              Отчет ВГБЛ-6 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-7_2016.pdf"
            >
              Отчет ВГБЛ-7 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-8_2016.pdf"
            >
              Отчет ВГБЛ-8 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-9_2016.pdf"
            >
              Отчет ВГБЛ-9 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-10_2016.pdf"
            >
              Отчет ВГБЛ-10 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_o_provedenii_vgtl-1_keno_sportloto_2016.pdf"
            >
              Отчет о&nbsp;проведении ВГТЛ-1&nbsp;КЕНО-Спортлото 2016 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_o_provedenii_vgtl-2_sportloto_6_iz_49_2016.pdf"
            >
              Отчет о&nbsp;проведении ВГТЛ-2 Спортлото 6&nbsp;из&nbsp;49 2016
              год
            </a>
          </li>
        </ul>
      </p>
      <p className="page__text">
        <ul>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/zakluchenie_sportloto_2015.pdf"
            >
              Аудиторское заключение за&nbsp;2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-1_2015.pdf"
            >
              Отчет ВГБЛ-1 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-2_2015.pdf"
            >
              Отчет ВГБЛ-2 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-3_2015.pdf"
            >
              Отчет ВГБЛ-3 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-4_2015.pdf"
            >
              Отчет ВГБЛ-4 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-5_2015.pdf"
            >
              Отчет ВГБЛ-5 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-6_2015.pdf"
            >
              Отчет ВГБЛ-6 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-7_2015.pdf"
            >
              Отчет ВГБЛ-7 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-8_2015.pdf"
            >
              Отчет ВГБЛ-8 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-9_2015.pdf"
            >
              Отчет ВГБЛ-9 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-10_2015.pdf"
            >
              Отчет ВГБЛ-10 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_o_provedenii_vgtl-1_keno_sportloto_2015.pdf"
            >
              Отчет о&nbsp;проведении ВГТЛ-1&nbsp;КЕНО-Спортлото 2015 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_o_provedenii_vgtl-2_sportloto_6_iz_49_2015.pdf"
            >
              Отчет о&nbsp;проведении ВГТЛ-2 Спортлото 6&nbsp;из&nbsp;49 2015
              год
            </a>
          </li>
        </ul>
      </p>
      <p className="page__text">
        <ul>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/zakluchenie_sportloto_2014.pdf"
            >
              Аудиторское заключение за&nbsp;2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-1_2014.pdf"
            >
              Отчет ВГБЛ-1 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-2_2014.pdf"
            >
              Отчет ВГБЛ-2 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-3_2014.pdf"
            >
              Отчет ВГБЛ-3 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-4_2014.pdf"
            >
              Отчет ВГБЛ-4 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-5_2014.pdf"
            >
              Отчет ВГБЛ-5 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-6_2014.pdf"
            >
              Отчет ВГБЛ-6 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-7_2014.pdf"
            >
              Отчет ВГБЛ-7 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-8_2014.pdf"
            >
              Отчет ВГБЛ-8 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-9_2014.pdf"
            >
              Отчет ВГБЛ-9 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_vgbl-10_2014.pdf"
            >
              Отчет ВГБЛ-10 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_o_provedenii_vgtl-1_keno_sportloto_2014.pdf"
            >
              Отчет о&nbsp;проведении ВГТЛ-1&nbsp;КЕНО-Спортлото 2014 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/otchet_o_provedenii_vgtl-2_sportloto_6_iz_49_2014.pdf"
            >
              Отчет о&nbsp;проведении ВГТЛ-2 Спортлото 6&nbsp;из&nbsp;49 2014
              год
            </a>
          </li>
        </ul>
      </p>
      <p className="page__text">
        <ul>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/Auditor_conclusion_2013.pdf"
            >
              Аудиторское заключение за&nbsp;2013 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/sportloto_annual_report_2013.pdf"
            >
              Годовой отчет за&nbsp;2013 год
            </a>
          </li>
        </ul>
      </p>
      <p className="page__text">
        <ul>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/Auditor_conclusion_2012.pdf"
            >
              Аудиторское заключение за&nbsp;2012 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/sportloto_annual_report_2012.pdf"
            >
              Годовой отчет за&nbsp;2012 год
            </a>
          </li>
        </ul>
      </p>
      <p className="page__text">
        <ul>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/Auditor_conclusion_2011.pdf"
            >
              Аудиторское заключение за&nbsp;2011 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/sportloto_annual_report_2011.pdf"
            >
              Годовой отчет за&nbsp;2011 год
            </a>
          </li>
        </ul>
      </p>
      <p className="page__text">
        <ul>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/Auditor_conclusion_2010.pdf"
            >
              Аудиторское заключение за&nbsp;2010 год
            </a>
          </li>
          <li>
            <a
              className="page__link page__link--pdf"
              href="/pdf/sportloto_annual_report_2010.pdf"
            >
              Годовой отчет за&nbsp;2010 год
            </a>
          </li>
        </ul>
      </p>
    </div>
  </article>
);

export default InfoDisclosure;
