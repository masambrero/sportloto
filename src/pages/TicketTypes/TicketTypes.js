import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import TicketSlider from '../../components/TicketSlider/TicketSlider';
import './TicketType.css';

const TicketTypes = () => (
  <article className="page page--ticket-types">
    <Helmet>
      <title>
        Билеты Спортлото всероссийских государственных бестиражных лотерей
      </title>
      <meta
        name="description"
        content="Билеты всероссийских государственных бестиражных лотерей  Спортлото – возможность выиграть до 5 000 000 рублей*. Лотерейные билеты  Спортлото можно приобрести в отделениях ОАО «Сбербанк России»."
      />
      <meta name="keywords" content="билеты лотереи, билеты спортлото" />
    </Helmet>
    <div className="page__text-block container">
      <h3 className="page__header">Виды лотерейных билетов</h3>
      <TicketSlider />
      <div className="information-tab">
        <h1 className="information-tab__header">
          Всероссийские государственные бестиражные лотереи
        </h1>
        <p className="information-tab__text">
          Каждую секунду кто-то покупает билет моментальной лотереи Спортлото.
          С&nbsp;2011 года по&nbsp;сегодняшний день продано более 170 миллионов
          билетов моментальных лотерей. При этом каждый четвертый билет
          оказывается выигрышным. Ежедневно победителям этих лотерей
          выплачивается примерно 1&nbsp;млн рублей!
        </p>
        <p className="information-tab__text">
          Билеты вы&nbsp;найдете в&nbsp;лотерейных киосках, магазинах
          &laquo;Связной&raquo;, офисах продаж &laquo;Мегафон&raquo;
          и&nbsp;отделениях почтовой связи. Билеты также продаются
          в&nbsp;букмекерских сетях 1XBet, &laquo;Лига ставок&raquo;,
          &laquo;БукмекерПаб&raquo;, спортбарах &laquo;Торнадо&raquo;,
          лотерейной сети &laquo;Балт-Лото&raquo;, в&nbsp;киосках
          &laquo;АиФ&raquo; и&nbsp;&laquo;Роспечать&raquo;.
        </p>
        <p className="information-tab__text">
          Цена лотерейных <b>билетов Спортлото</b>&nbsp;&mdash;
          от&nbsp;20&nbsp;до&nbsp;100&nbsp;рублей.
        </p>
        <p className="information-tab__text">
          <Link to="/faq" className="information-tab__link">
            Вопросы и&nbsp;ответы
          </Link>
          <Link to="/buy" className="information-tab__link">
            Где купить?
          </Link>
        </p>
      </div>
    </div>
  </article>
);

export default TicketTypes;
