import React from 'react';

const SportHoliday = () => (
  <article className="page page--faster-higher-stronger">
    <div className="page__text-block container">
      <h3 className="page__header">Лотерея &laquo;Праздник спорта&raquo;</h3>
      <p className="page__text">
        Номинал билета <b>25&nbsp;рублей</b>.
      </p>
      <p className="page__text">
        Всероссийская государственная бестиражная лотерея &laquo;Праздник
        спорта&raquo;. Максимальный выигрыш составляет 500&nbsp;000&nbsp;рублей.
        Призовой фонд 50%.
      </p>
      <p className="page__text">
        Номиналы возможных выигрышей: 25, 100, 200, 1500, 3000, 50&nbsp;000,
        500&nbsp;000&nbsp;рублей.
      </p>
      <p className="page__text">
        Лотерейный билет состоит из&nbsp;двух игровых полей&nbsp;&mdash;
        &laquo;Игра&nbsp;1&raquo; и&nbsp;&laquo;Игра&nbsp;2&raquo;.
      </p>
      <p className="page__text">
        <b>&laquo;Игра&nbsp;1&raquo;</b>. Сотрите защитный слой в&nbsp;игровом
        поле. Выигрыш соответствует указанному числу в&nbsp;рублях.
      </p>
      <p className="page__text">
        <b>&laquo;Игра&nbsp;2&raquo;</b>. Сотрите защитный слой в&nbsp;игровом
        поле. Если под защитным слоем содержится символ &laquo;х1&raquo;,
        то&nbsp;выигрыш в&nbsp;&laquo;Игре&nbsp;1&raquo; не&nbsp;изменится. Если
        содержится символ &laquo;х2&raquo;, выигрыш
        в&nbsp;&laquo;Игре&nbsp;1&raquo; увеличивается в&nbsp;два раза.<br />
        Применяется таблица распределения призового фонда &#8470;&nbsp;2.
      </p>
      <p className="page__text">
        <a href="/pdg/l_6.pdf" className="page__link" target="_blank">
          Условия лотереи
        </a>
      </p>
    </div>
  </article>
);

export default SportHoliday;
