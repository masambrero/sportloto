import React from 'react';

const RussianGames = () => (
  <article className="page page--faster-higher-stronger">
    <div className="page__text-block container">
      <h3 className="page__header">Лотерея &laquo;Русские игры&raquo;</h3>
      <p className="page__text">
        Номинал билета <b>30&nbsp;рублей</b>.
      </p>
      <p className="page__text">
        Всероссийская государственная бестиражная лотерея &laquo;Русские
        игры&raquo;. Максимальный выигрыш составляет 300&nbsp;000&nbsp;рублей.
        Призовой фонд 50%.
      </p>
      <p className="page__text">
        Номиналы возможных выигрышей: 30, 60, 150, 300, 1500, 15&nbsp;000,
        300&nbsp;000&nbsp;рублей.
      </p>
      <p className="page__text">
        Лотерейный билет состоит из&nbsp;двух игровых полей:
        &laquo;Игра&nbsp;1&raquo; и&nbsp;&laquo;Игра&nbsp;2&raquo;.
      </p>
      <p className="page__text">
        &laquo;Игра&nbsp;1&raquo;. Под защитным слоем располагаются три символа,
        каждый из&nbsp;которых соответствует одному из&nbsp;выигрышей. Если все
        три символа одинаковые, то&nbsp;билет является выигрышным. Выигрыш равен
        повторяющемуся символу.
      </p>
      <p className="page__text">
        &laquo;Игра&nbsp;2&raquo;. Под защитным слоем располагается одна
        из&nbsp;следующих букв: &laquo;С&raquo;, &laquo;Т&raquo;,
        &laquo;А&raquo;, &laquo;Р&raquo;. Собрав комбинацию из&nbsp;пяти
        билетов, под защитным слоем которых располагаются буквы, из&nbsp;которых
        можно составить слово &laquo;СТАРТ&raquo;, участник получит
        гарантированный выигрыш 1500&nbsp;рублей.
      </p>
      <p className="page__text">
        Внимание! Получение гарантированного выигрыша по&nbsp;комбинации билетов
        возможно только в&nbsp;том случае, если ни&nbsp;в&nbsp;одном
        из&nbsp;пяти билетов, содержащих буквы, образующие слово
        &laquo;СТАРТ&raquo;, не&nbsp;был получен выигрыш в&nbsp;первом игровом
        поле (&laquo;Игра&nbsp;1&raquo;). Одновременно с&nbsp;получением
        гарантированного выигрыша по&nbsp;комбинации билетов будут получены
        выигрыши в&nbsp;первом игровом поле (&laquo;Игра&nbsp;1&raquo;)
        во&nbsp;всех пяти билетах.
      </p>
      <p className="page__text">
        Применяется таблица распределения призового фонда &#8470;&nbsp;2.
      </p>
      <p className="page__text">
        <a href="/pdg/l_3.pdf" className="page__link" target="_blank">
          Условия лотереи
        </a>
      </p>
    </div>
  </article>
);

export default RussianGames;
