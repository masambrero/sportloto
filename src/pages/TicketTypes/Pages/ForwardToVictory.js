import React from 'react';

const ticketImg = require('../../../assets/imgs/tickets/ticket_10.png');

const ForwardToVictory = () => (
  <article className="page page--faster-higher-stronger">
    <div className="page__text-block container">
      <h3 className="page__header">
        Лотерея &laquo;Вперед к&nbsp;победе&raquo;
      </h3>
      <div>
        <img
          className="page__ticket-img"
          src={ticketImg}
          alt="lottery ticket"
        />
      </div>
      <p className="page__text">
        Номинал билета <b>50&nbsp;рублей</b>.<br />
        Всероссийская государственная бестиражная лотерея &laquo;Вперед
        к&nbsp;победе&raquo;. Максимальный выигрыш составляет
        3&nbsp;000&nbsp;000&nbsp;рублей. Призовой фонд 50%.<br />
        Номиналы возможных выигрышей: 50, 200, 400, 1000, 10&nbsp;000,
        20&nbsp;000, 100&nbsp;000, 3&nbsp;000&nbsp;000&nbsp;рублей.<br />
        Также игра представлена в&nbsp;дизайне &laquo;Пингвины&raquo;, номиналы
        возможных выигрышей которого: 50, 100, 200, 300, 500, 5000, 10&nbsp;000,
        20&nbsp;000, 1&nbsp;000&nbsp;000&nbsp;рублей.<br />
        Лотерейный билет состоит из&nbsp;четырех игровых полей: &laquo;Ваш
        результат&raquo;, &laquo;Результат соперника&raquo;, &laquo;Приз&raquo;
        и&nbsp;&laquo;Игра&nbsp;2&raquo;, которые образуют две игры.<br />
        <b>&laquo;Игра&nbsp;1&raquo;</b>. Необходимо, чтобы показатель поля
        &laquo;Ваш результат&raquo; оказался выше показателя поля
        &laquo;Результат соперника&raquo;. Под полем &laquo;Приз&raquo;
        содержится информация о&nbsp;вашем возможном выигрыше.<br />
        <b>&laquo;Игра&nbsp;2&raquo;</b>. Под защитным слоем располагается одна
        из&nbsp;следующих букв: &laquo;С&raquo;, &laquo;О&raquo;,
        &laquo;Ч&raquo;, &laquo;И&raquo;. Собрав комбинацию из&nbsp;четырёх
        билетов, под защитным слоем которых располагаются буквы, из&nbsp;которых
        можно составить слово &laquo;СОЧИ&raquo;, вы&nbsp;получите
        гарантированный выигрыш 20&nbsp;000&nbsp;рублей.<br />
        Внимание! Получение гарантированного выигрыша по&nbsp;комбинации билетов
        возможно только в&nbsp;том случае, если ни&nbsp;в&nbsp;одном
        из&nbsp;четырех билетов, содержащих буквы, образующие слово
        &laquo;СОЧИ&raquo;, не&nbsp;был получен выигрыш, содержащийся под полем
        &laquo;Приз&raquo;. Одновременно с&nbsp;получением выигрыша
        по&nbsp;комбинации билетов будут получены выигрыши, содержащиеся под
        словом &laquo;Приз&raquo;, во&nbsp;всех четырех билетах.<br />
        Применяется таблица распределения призового фонда
        &#8470;&nbsp;2&nbsp;(для дизайна &laquo;Пингвинов&raquo;&nbsp;&mdash;
        таблица &#8470;&nbsp;1).<br />
      </p>
      <p className="page__text">
        <a href="/pdg/l_9.pdf" className="page__link" target="_blank">
          Условия лотереи
        </a>
      </p>
    </div>
  </article>
);

export default ForwardToVictory;
