import React from 'react';

const MerryStarts = () => (
  <article className="page page--faster-higher-stronger">
    <div className="page__text-block container">
      <h3 className="page__header">Лотерея &laquo;Веселые старты&raquo;</h3>
      <p className="page__text">
        Номинал билета <b>20&nbsp;рублей</b>.
      </p>
      <p className="page__text">
        Всероссийская государственная бестиражная лотерея &laquo;Веселые
        старты&raquo;. Максимальный выигрыш составляет 200&nbsp;000&nbsp;рублей.
        Призовой фонд 50%.
      </p>
      <p className="page__text">
        Номиналы возможных выигрышей: 20, 40, 100, 200, 1000, 10&nbsp;000
        и&nbsp;200&nbsp;000&nbsp;рублей.
      </p>
      <p className="page__text">
        Сотрите защитный слой в&nbsp;игровом поле. Под защитным слоем
        вы&nbsp;найдете либо символ, соответствующий одному из&nbsp;выигрышей,
        либо символ, информирующий об&nbsp;отсутствии выигрыша.
      </p>
      <p className="page__text">
        Применяется таблица распределения призового фонда &#8470;&nbsp;2.
      </p>
      <p className="page__text">
        <a href="/pdg/l_3.pdf" className="page__link" target="_blank">
          Условия лотереи
        </a>
      </p>
    </div>
  </article>
);

export default MerryStarts;
