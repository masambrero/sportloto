import React from 'react';

const contest = require('../../../assets/imgs/tickets/bvs_2.png');
const box = require('../../../assets/imgs/tickets/bvs_1.jpg');

const FasterHigherStronger = () => (
  <article className="page page--faster-higher-stronger">
    <div className="page__text-block container">
      <h3 className="page__header">Лотерея &laquo;Спортивный сезон&raquo;</h3>
      <p className="page__text">
        Номинал билета <b>100&nbsp;рублей</b>.<br />
        Всероссийская государственная бестиражная лотерея &laquo;Быстрее, выше,
        сильнее!&raquo;. Максимальный выигрыш составляет
        5&nbsp;000&nbsp;000&nbsp;рублей. Призовой фонд 50%.<br />
        Номиналы возможных выигрышей: 100, 200, 1000, 10&nbsp;000, 50&nbsp;000,
        500&nbsp;000, 5&nbsp;000&nbsp;000&nbsp;рублей.<br />
        Представлена в&nbsp;дизайне &laquo;Бокс&raquo;
        и&nbsp;&laquo;Конкур&raquo;.<br />
        Лотерейный билет состоит из&nbsp;двух игровых полей:
        &laquo;Игра&nbsp;1&raquo; и&nbsp;&laquo;Игра&nbsp;2&raquo;.<br />
        <b>&laquo;Игра&nbsp;1&raquo;</b>. Под защитным слоем располагаются три
        символа, каждый из&nbsp;которых соответствует одному из&nbsp;выигрышей.
        Если все три символа одинаковые, то&nbsp;билет является выигрышным.
        Выигрыш равен повторяющемуся символу.<br />
      </p>
      <div>
        <figure className="page__ticket-figure">
          <div>
            <img src={contest} alt="lottery ticket" />
          </div>
          <figcaption>
            Соответствие символов выигрышу для &laquo;Конкура&raquo;
          </figcaption>
        </figure>
        <figure className="page__ticket-figure">
          <div>
            <img src={box} alt="lottery ticket" />
          </div>
          <figcaption>
            Соответствие символов выигрышу для &laquo;Бокса&raquo;
          </figcaption>
        </figure>
      </div>
      <p className="page__text">
        Cоответствие символов выигрышу для &laquo;Конкура&raquo; Соответствие
        символов выигрышу для &laquo;Бокса&raquo;<br />
        <b>&laquo;Игра&nbsp;2&raquo;</b>. Под защитным слоем располагается одна
        из&nbsp;следующих букв: &laquo;Р&raquo;, &laquo;Е&raquo;,
        &laquo;К&raquo;, &laquo;О&raquo;, &laquo;Д&raquo;. Собрав комбинацию
        из&nbsp;шести билетов, под защитным слоем которых располагаются буквы,
        из&nbsp;которых можно составить слово &laquo;РЕКОРД&raquo;,
        вы&nbsp;получите гарантированный выигрыш 10&nbsp;000&nbsp;рублей.<br />
        <b>Внимание!</b> Получение гарантированного выигрыша по&nbsp;комбинации
        билетов возможно только в&nbsp;том случае, если ни&nbsp;в&nbsp;одном
        из&nbsp;шести билетов, содержащих буквы, образующие слово
        &laquo;РЕКОРД&raquo;, не&nbsp;был получен выигрыш в&nbsp;
        <b>Игре&nbsp;1</b>. Одновременно с&nbsp;получением выигрыша
        по&nbsp;комбинации билетов будут получены выигрыши в&nbsp;<b>Игре 1</b>&nbsp;во
        всех шести билетах.<br />
        Применяется таблица распределения призового фонда &#8470;&nbsp;1.
      </p>
      <p className="page__text">
        <a href="/pdg/l_1.pdf" className="page__link" target="_blank">
          Условия лотереи
        </a>
      </p>
    </div>
  </article>
);

export default FasterHigherStronger;
