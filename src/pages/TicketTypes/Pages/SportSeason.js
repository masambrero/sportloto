import React from 'react';

const ticketImg = require('../../../assets/imgs/tickets/ticket_8.png');

const SportSeason = () => (
  <article className="page page--faster-higher-stronger">
    <div className="page__text-block container">
      <h3 className="page__header">Лотерея &laquo;Спортивный сезон&raquo;</h3>
      <div>
        <img
          className="page__ticket-img"
          src={ticketImg}
          alt="lottery ticket"
        />
      </div>
      <p className="page__text">
        Номинал билета <b>25&nbsp;рублей</b>.<br />
        Всероссийская государственная бестиражная лотерея &laquo;Спортивный
        сезон&raquo;. Максимальный выигрыш составляет 250&nbsp;000&nbsp;рублей.
        Призовой фонд&nbsp;50%.
      </p>
      <p className="page__text">
        Номиналы возможных выигрышей: 25, 50, 100, 200, 500, 1000, 2000,
        250&nbsp;000&nbsp;рублей.
      </p>
      <p className="page__text">
        Лотерейный билет состоит из&nbsp;двух игровых полей&nbsp;&mdash;
        &laquo;Игра&nbsp;1&raquo; и&nbsp;&laquo;Игра&nbsp;2&raquo;.
      </p>
      <p className="page__text">
        &laquo;Игра&nbsp;1&raquo;. Под защитным слоем в&nbsp;игровом окне
        содержатся три надписи, каждая из&nbsp;которых соответствует одному
        из&nbsp;выигрышей. Билет является выигрышным в&nbsp;данной игре, если
        все три надписи одинаковые. Выигрыш соответствует повторяющейся надписи
      </p>
      <p className="page__text">
        &laquo;Игра&nbsp;2&raquo;. Под защитным слоем содержится символ
        &laquo;х1&raquo;, выигрыш в&nbsp;игре 1&nbsp;не изменяется. Если
        содержится символ &laquo;х2&raquo;, выигрыш в&nbsp;игре
        1&nbsp;увеличивается в&nbsp;два раза.
      </p>
      <p className="page__text">
        <a href="/pdg/l_9.pdf" className="page__link" target="_blank">
          Условия лотереи
        </a>
      </p>
    </div>
  </article>
);

export default SportSeason;
