import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import './Right.css';

const Right = () => (
  <article className="page page--right">
    <Helmet>
      <title>Правовая информация для пользователей сайта Спортлото</title>
      <meta
        name="description"
        content="Правовая информация для пользователей официального сайта ООО «Спортлото» - Раскрытие информации"
      />
      <meta name="keywords" content="сайт лотереи" />
    </Helmet>
    <div className="page__text-block container">
      <h3 className="page__header">
        Правовая информация для пользователей официального сайта ООО
        &laquo;Спортлото&raquo;
      </h3>
      <ul className="page__text page__text--list">
        <li>
          ООО &laquo;Спортлото&raquo; является владельцем Интернет Сайта{' '}
          <a className="page__link" href="http://www.sportloto.ru">
            www.sportloto.ru
          </a>{' '}
          (далее&nbsp;&mdash; &laquo;Сайт&raquo;)
        </li>
        <li>
          Пользователем Сайта является любое лицо, которое зашло через сеть
          Интернет на&nbsp;Сайт.
        </li>
        <li>
          Имя и&nbsp;логотип ООО &laquo;Спортлото&raquo; могут быть использованы
          исключительно по&nbsp;ограниченной лицензии, предоставляемой ООО
          &laquo;Спортлото&raquo; на&nbsp;основании Договора.
        </li>
        <li>
          ООО &laquo;Спортлото&raquo; не&nbsp;дает гарантии точности, полноты,
          адекватности воспроизведения третьими лицами любой информации,
          размещенной на&nbsp;настоящем сайте, и&nbsp;в&nbsp;безоговорочной
          форме отказывается от&nbsp;ответственности за&nbsp;ошибки
          и&nbsp;упущения, допущенные третьими лицами при воспроизведении такой
          информации.
        </li>
        <li>
          ООО &laquo;Спортлото&raquo; не&nbsp;несет ответственности
          ни&nbsp;за&nbsp;какие убытки, включая реальный ущерб и&nbsp;упущенную
          выгоду, возникшие в&nbsp;связи с&nbsp;настоящим сайтом, его
          использованием, невозможностью использования, помехами, дефектами,
          вирусами или сбоем в&nbsp;работе системного оборудования или
          программного обеспечения Сайта.
        </li>
        <li>
          Вводя какие-либо данные на&nbsp;настоящем сайте пользователь
          подтверждает свое безусловное согласие на&nbsp;обработку, передачу
          и&nbsp;использование ООО &laquo;Спортлото&raquo; любых данных
          пользователя, включая его персональные данные, введенные при
          регистрации на&nbsp;Сайте или переданные при обращении в&nbsp;ООО
          &laquo;Спортлото&raquo;, осознавая и&nbsp;соглашаясь, что обработка
          данных осуществляется по&nbsp;усмотрению ООО &laquo;Спортлото&raquo;
          любыми способами, допустимыми действующим законодательством Российской
          Федерации, в&nbsp;том числе с&nbsp;применением средств шифрования.
        </li>
        <li>
          <Link className="page__link" to="/raskrytie_informatsii">
            Раскрытие информации
          </Link>
        </li>
      </ul>
      <p className="page__text">
        <a
          className="page__link page__link--pdf"
          href="/pdf/Uslovia VGTL-1 KENO-Sportloto.pdf"
          target="_blank"
        >
          Условия ВГТЛ-1 &laquo;КЕНО-Спортлото&raquo;
        </a>
        <br />
        <a
          className="page__link page__link--pdf"
          href="/pdf/Uslovia VGTL-2 Sportloto 6 out of 49.pdf"
          target="_blank"
        >
          Условия ВГТЛ-1 &laquo;КЕНО-Спортлото&raquo;
        </a>
      </p>
    </div>
  </article>
);

export default Right;
