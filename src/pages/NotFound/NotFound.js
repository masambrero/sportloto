import React from 'react';
import { Helmet } from 'react-helmet';

const NotFound = () => (
  <article className="page page--news">
    <Helmet>
      <title>Страница не найдена</title>
      <meta
        name="keywords"
        content="Спортлото, Лотерея, Моментальная лотетея, Выиграть в лотерею"
      />
    </Helmet>
    <div className="page__text-block container">
      <h3 className="page__header">Страница не&nbsp;найдена</h3>
      <p className="page__text">Запрашиваемая страница не&nbsp;найдена!</p>
    </div>
  </article>
);

export default NotFound;
