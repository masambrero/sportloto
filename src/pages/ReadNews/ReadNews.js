import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { PropTypes } from 'prop-types';
import { removeNewLinesFromText } from '../../utils';
import './ReadNews.css';

export default class ReadNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: true,
    };
  }

  componentDidMount() {
    this.fetchNews();
  }

  fetchNews = () => {
    const { id } = this.props.match.params;
    fetch(`https://api.stoloto.ru/mobile/api/v20/service/news/sportloto/${id}`)
      .then((response) => {
        if (response.status !== 200) {
          throw response.statusText;
        }
        return response;
      })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        this.setState({ news: json, isFetching: false });
      })
      .catch((err) => {
        this.setState({ isFetching: false });
        console.log(err);
      });
  };

  render() {
    const { news, isFetching } = this.state;
    return (
      <article className="page page--read-news">
        {news && <Helmet>{news.title && <title>{news.title}</title>}</Helmet>}
        <div
          className="container"
          style={{
            transition: 'opacity .2s ease',
            opacity: isFetching ? 0 : 1,
          }}
        >
          {news && (
            <div className="news-page">
              <div className="news-page__header">
                {news.date && (
                <time className="news-page__date">{news.date}</time>
                )}
                <Link className="news-page__nav" to="/news">
                  Вернуться назад
                </Link>
              </div>
              {news.name && (
              <h3
                className="news-page__title"
                dangerouslySetInnerHTML={{
                  __html: removeNewLinesFromText(news.name),
                }}
              />
              )}
              {news.text && (
              <p
                className="news-page__text"
                dangerouslySetInnerHTML={{
                  __html: removeNewLinesFromText(news.text),
                }}
              />
              )}
            </div>
          )}
        </div>
      </article>
    );
  }
}

ReadNews.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};
