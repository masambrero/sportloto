import React from 'react';
import { Helmet } from 'react-helmet';
import LotteryTab from '../../components/LotteryTab/LotteryTab';
import './About.css';

const About = () => (
  <article className="page page--about">
    <Helmet>
      <title>О компании</title>
      <meta
        name="description"
        content="Информация о компании Спортлото. Государственные лотереи"
      />
      <meta name="keywords" content="Спортлото, Официальный сайт Спортлото" />
    </Helmet>
    <div className="page__info-block page__info-block--shifted container">
      <div className="page__text-block">
        <h3 className="page__header">Информация о&nbsp;компании</h3>
        <p className="page__text">
          ООО &laquo;Спортлото&raquo; является оператором государственных
          лотерей, проводимых в&nbsp;поддержку развития спорта высших достижений
          и&nbsp;системы подготовки спортивного резерва на&nbsp;основании
          Распоряжения Правительства Российской Федерации от&nbsp;14.09.2009
          &#8470;&nbsp;1318-р.
        </p>
        <p className="page__text">
          Организатор лотерей: Министерство Финансов Российской Федерации.
        </p>
        <p className="page__text">
          Наша цель&nbsp;&mdash; возрождение традиций лотерей, в&nbsp;которых
          каждый участник делает вклад в&nbsp;развитие национального спорта
          и&nbsp;может выиграть крупный приз. Спортлото предлагает Вам стать
          участником увлекательных и&nbsp;захватывающих всероссийских тиражных
          лотерей &laquo;Спортлото Матчбол&raquo; и&nbsp;&laquo;КЕНО
          Спортлото&raquo;, а&nbsp;также бестиражных лотерей.
        </p>
        <p className="page__text">
          50% всех средств, вырученных от&nbsp;продажи лотерейных билетов
          составляют призовой фонд лотерей, из&nbsp;которого выплачиваются
          выигрыши победителям.
        </p>
        <p className="page__text">
          Целевые отчисления от&nbsp;проведения лотереи направляются
          в&nbsp;бюджет Российской Федерации.
        </p>
        <p className="page__text page__text--description">
          Сайт{' '}
          <a className="page__link" href="//www.sportloto.ru">
            sportloto.ru
          </a>{' '}
          является зарегистрированным Средством массовой информации&nbsp;&mdash;
          Спортлото. Государственные Лотереи. Свидетельство о&nbsp;регистрации
          Средства массовой информации: Эл &#8470; ФС77-54339
          от&nbsp;29.05.2013&nbsp;г. Зарегистрировано в&nbsp;Федеральной службе
          по&nbsp;надзору в&nbsp;сфере связи, информационных технологий
          и&nbsp;массовых коммуникаций (Роскомнадзор). Учредитель СМИ: ООО
          &laquo;Спортлото&raquo;, ОГРН: 1107746125607 . Адрес редакции СМИ:
          109316, г. Москва, Волгоградский проспект, д.&nbsp;43, корп.&nbsp;3
        </p>
      </div>
      <div className="page__lotteries page__lotteries--vertical">
        <LotteryTab
          listDirection="vertical"
          headerText="Узнать больше о&nbsp;лотереях"
          hasLotteryLink
        />
      </div>
    </div>
  </article>
);

export default About;
