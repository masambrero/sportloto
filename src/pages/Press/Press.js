import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import LotteryTab from '../../components/LotteryTab/LotteryTab';
import './Press.css';

const sportlotoLogo = require('../../assets/imgs/logo.gif');
const kenoLogo = require('../../assets/imgs/keno.png');
const sixfournineLogo = require('../../assets/imgs/649.png');

const Press = () => (
  <article className="page page--press">
    <Helmet>
      <title>Пресс-центр</title>
      <meta
        name="description"
        content="Контактная информация спортлото для журналистов"
      />
      <meta name="keywords" content="Спортлото, пресс-центр" />
    </Helmet>
    <div className="page__info-block page__info-block--shifted container">
      <div className="page__text-block">
        <h3 className="page__header">Пресс-центр</h3>
        <h4 className="page__subheader">
          <Link className="page__link" to="/news">
            Новости Спортлото
          </Link>
        </h4>
        <p className="page__text">
          Основные принципы деятельности ООО
          &laquo;Спортлото&raquo;&nbsp;&mdash; прозрачность
          и&nbsp;информационная открытость.
          <br />
          Мы&nbsp;с&nbsp;готовностью ответим на&nbsp;Ваши вопросы
          и&nbsp;предоставим актуальную информации о&nbsp;компании.
          <br />
          Контакт для журналистов:{' '}
          <a className="page__link" href="mailto:press@sportloto.ru">
            press@sportloto.ru
          </a>
        </p>
        <div className="page__logo-block logo-list">
          <div className="logo-list__item">
            <img
              className="logo-list__img"
              src={sportlotoLogo}
              alt="sportloto logo"
            />
            <a className="logo-list__link" href={sportlotoLogo} target="_blank">
              Cкачать логотип
            </a>
          </div>
          <div className="logo-list__item">
            <img className="logo-list__img" src={kenoLogo} alt="keno logo" />
            <a className="logo-list__link" href={kenoLogo} target="_blank">
              Cкачать логотип
            </a>
          </div>
          <div className="logo-list__item">
            <img
              className="logo-list__img"
              src={sixfournineLogo}
              alt="6x49 logo"
            />
            <a
              className="logo-list__link"
              href={sixfournineLogo}
              target="_blank"
            >
              Cкачать логотип
            </a>
          </div>
        </div>
      </div>
      <div className="page__lotteries page__lotteries--vertical">
        <LotteryTab
          listDirection="vertical"
          headerText="Узнать больше о&nbsp;лотереях"
          hasLotteryLink
        />
      </div>
    </div>
  </article>
);

export default Press;
