import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import BorderBlock from '../../components/BorderBlock/BorderBlock';
import NewsBlock from '../../components/NewsBlock/NewsBlock';
import ScrollArrow from '../../components/ScrollArrow/ScrollArrow';
import './News.css';

export default class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: true,
      newsList: [],
    };
  }

  componentDidMount() {
    this.fetchNews();
  }

  fetchNews = () => {
    fetch('https://api.stoloto.ru/mobile/api/v20/service/news/sportloto')
      .then((response) => {
        if (response.status !== 200) {
          throw response.statusText;
        }
        return response;
      })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        this.setState({ newsList: json, isFetching: false });
      })
      .catch((err) => {
        this.setState({ isFetching: false });
        console.log(err);
      });
  };

  render() {
    const { newsList, isFetching } = this.state;
    return (
      <article className="page page--news">
        <Helmet>
          <title>Новости</title>
          <meta
            name="description"
            content="Новости официального сайта лотереи Спортлото"
          />
          <meta
            name="keywords"
            content="Лотерея спортлото, новости официального сайта"
          />
        </Helmet>
        <div className="container">
          <h3 className="page__header">Новости</h3>
          <div
            className="page__news-list news-list"
            style={{
              transition: 'opacity .2s ease',
              opacity: isFetching ? 0 : 1,
            }}
          >
            {newsList.map((i, index) => (
              <div className="news-list__entry" key={index}>
                <BorderBlock
                  hasLink
                  hasPadding
                  hasBottomBorder
                  linkText="Прочитать новость"
                  linkUrl={`/read_news/${i.id}`}
                  isInnerLink
                  borderType="left"
                >
                  <NewsBlock news={i} />
                </BorderBlock>
              </div>
            ))}
          </div>
          <div className="page__scroll-arrow">
            <ScrollArrow />
          </div>
        </div>
      </article>
    );
  }
}
