import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { formatDate, formatNumber } from '../../utils';
import './Keno.css';

const kenoLogo = require('../../assets/imgs/keno.png');

export default class Keno extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: true,
      number: null,
      date: null,
      ticketPrice: null,
      superPrize: null,
    };
  }

  componentDidMount() {
    this.fetchDrawInfo();
  }

  fetchDrawInfo = () => {
    fetch('https://api.stoloto.ru/mobile/api/v20/service/games/info')
      .then((response) => {
        if (response.status !== 200) {
          throw response.statusText;
        }
        return response;
      })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        if (!json.games) throw json;
        json.games.map((i) => {
          if (i.name === 'keno') {
            return this.setState({
              isFetching: false,
              number: i.draw.number,
              date: i.draw.date,
              ticketPrice: i.draw.ticketPrice,
              superPrize: i.draw.superPrize,
            });
          }
          return false;
        });
      })
      .catch((err) => console.log(err));
  };

  render() {
    const { isFetching, number, date, ticketPrice, superPrize } = this.state;
    return (
      <article className="page page--schedule">
        <Helmet>
          <title>Спортлото: Переезд на сайт stoloto.ru</title>
          <meta
            name="description"
            content="Описание тиражной лотереи. Результаты проведения розыгрыша тиража. Условия проведения лотереи. Проверить результаты розыгрыша последнего тиража."
          />
        </Helmet>
        <div className="lottery-schedule container">
          <div className="lottery-schedule__logo">
            <img alt="Спортлото «Кено»" src={kenoLogo} />
          </div>
          <div
            className="lottery-schedule__info-block"
            style={{
              transition: 'opacity .2s ease',
              opacity: isFetching ? 0 : 1,
            }}
          >
            <h1 className="lottery-schedule__header">
              Время проведения розыгрышей &laquo;КЕНО-Спортлото&raquo;
            </h1>
            <p className="lottery-schedule__text">
              Следующий тираж &laquo;КЕНО-Спортлото&raquo; &#8470;{number}{' '}
              пройдет {formatDate(date)}. Время московское.
            </p>
            <p className="lottery-schedule__text">
              Выигрыш от&nbsp;60&nbsp;рублей
            </p>
            <p className="lottery-schedule__text">
              Суперприз&nbsp;{formatNumber(superPrize)}&nbsp;рублей
            </p>
            <p className="lottery-schedule__text">
              Максимальный выигрыш в&nbsp;лотерею составляет
              10&nbsp;000&nbsp;000&nbsp;рублей.
            </p>
            <p className="lottery-schedule__text">
              Чтобы стать участником, отметьте в&nbsp;билете
              от&nbsp;1&nbsp;до&nbsp;10&nbsp;чисел в&nbsp;диапазоне
              от&nbsp;1&nbsp;до&nbsp;80
            </p>
            <p className="lottery-schedule__text">
              Билет от&nbsp;{ticketPrice}&nbsp;рублей
            </p>
            <p className="lottery-schedule__text">
              <a href="https://www.stoloto.ru/quickgames/keno">Купить билет!</a>
            </p>
            <p className="lottery-schedule__text">
              <a href="https://www.stoloto.ru/keno/archive">Архив тиражей</a>
            </p>
            <h2 className="lottery-schedule__header">
              Информация о&nbsp;проведении лотерей с&nbsp;использованием
              электронных лотерейных билетов.<br />
              Настоящим ООО &laquo;Спортлото&raquo; информирует
              об&nbsp;использовании электронных лотерейных билетов на&nbsp;сайте
              www.stoloto.ru. для следующей лотереи:
            </h2>
            <p className="lottery-schedule__text">
              Распоряжение Правительства РФ&nbsp;от&nbsp;14.09.2009&nbsp;г.
              &#8470;&nbsp;1318-р
            </p>
            <p className="lottery-schedule__text">
              Оператор: ООО &laquo;Спортлото&raquo;, тел.: +7&nbsp;495 204-87-78
            </p>
            <p className="lottery-schedule__text">&laquo;ВГТЛ-1&raquo;</p>
            <p className="lottery-schedule__text">Организатор: Минфин России</p>
            <p className="lottery-schedule__text">
              Призовой фонд&nbsp;&mdash; 50% от&nbsp;выручки.
            </p>
            <p className="lottery-schedule__text">
              Место проведения розыгрыша тиража&nbsp;&mdash; г. Москва.
            </p>
            <p className="lottery-schedule__text">
              Результаты тиража публикуются на&nbsp;сайтах www.stoloto.ru
              и&nbsp;www.lotonews.ru в&nbsp;течение 10&nbsp;дней после его
              проведения.
            </p>
            <p className="lottery-schedule__text">
              Выплата выигрышей осуществляется в&nbsp;точках распространения
              и&nbsp;оператором не&nbsp;позднее 30&nbsp;дней с&nbsp;даты
              проведения соответствующего тиража и&nbsp;продолжается
              не&nbsp;менее шести месяцев с&nbsp;момента опубликования
              в&nbsp;СМИ результатов данного тиража.
            </p>
            <p className="lottery-schedule__text">
              Дата проведения тиража: {formatDate(date)}. Время московское.
            </p>
            <p className="lottery-schedule__text">
              Билет считается выигрышным в&nbsp;первом туре, если его номер
              совпал с&nbsp;девятизначной выигрышной комбинацией первого тура
              в&nbsp;точном порядке слева направо.
            </p>
            <p className="lottery-schedule__text">
              Если номер билета содержит менее 9&nbsp;разрядов,
              то&nbsp;недостающие разряды считаются стоящими слева
              и&nbsp;равными нулю. Розыгрыш во&nbsp;втором туре основан
              на&nbsp;совпадении чисел, размещенных на&nbsp;билете,
              с&nbsp;числами выигрышной комбинации второго тура. Размер выигрыша
              и&nbsp;порядок его получения определяется в&nbsp;соответствии
              с&nbsp;условиями лотереи.
            </p>
            <p className="lottery-schedule__text">
              Цена единичной лотерейной ставки: 60&nbsp;руб.
            </p>
          </div>
        </div>
      </article>
    );
  }
}
