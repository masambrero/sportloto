import React from 'react';
import { Helmet } from 'react-helmet';
import LotteryTab from '../../components/LotteryTab/LotteryTab';
import ContactForm from '../../components/ContactForm/ContactForm';
import './Partnership.css';

const Partnership = () => (
  <article className="page page--partnership">
    <Helmet>
      <title>Сотрудничество - Спортлото</title>
      <meta
        name="description"
        content="Информация о сотрудничестве с компанией Спортлото государственные лотереи"
      />
      <meta
        name="keywords"
        content="Спортлото, Сотрудничество, Лотерея, Сеть продаж"
      />
    </Helmet>
    <div className="page__info-block page__info-block--shifted container">
      <div className="page__text-block">
        <h3 className="page__header">Сотрудничество</h3>
        <p className="page__text">
          Спортлото расширяет сеть продаж и&nbsp;приглашает
          к&nbsp;сотрудничеству распространителей.
          <br />
          Станьте партнером Спортлото, для этого заполните анкету, и&nbsp;наш
          сотрудник свяжется с&nbsp;Вами в&nbsp;ближайшее время
        </p>
        <ContactForm />
      </div>
      <div className="page__lotteries page__lotteries--vertical">
        <LotteryTab
          listDirection="vertical"
          headerText="Узнать больше о&nbsp;лотереях"
          hasLotteryLink
        />
      </div>
    </div>
  </article>
);

export default Partnership;
