import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import './Faq.css';

const Faq = () => (
  <article className="page page--faq">
    <Helmet>
      <title>Моментальные лотереи - Вопросы и ответы</title>
      <meta
        name="description"
        content="Вопросы и ответы о тиражных лотереях Спортлото, как работает система КЕНО-Спортлото, когда проходят розыгрыши тиражей лотерей Спортлото, как сделать лотерейную ставку и как узнать результаты тиражей Спортлото, розыгрыши тиражей Спортлото, система КЕНО-Спортлото, оплата ставок и получение выигрышей"
      />
      <meta name="keywords" content="система кено, системы спортлото" />
    </Helmet>
    <div className="container">
      <h3 className="page__header">Вопросы и&nbsp;ответы</h3>
      <h4 className="page__subheader">
        Все часто задаваемые вопросы и&nbsp;ответы относительно лотерей
        Спортлото: система КЕНО-Спортлото, процедура получения выигрыша,
        результаты розыгрышей тиражей
      </h4>
      <div className="page__text-block">
        <div className="faq-block">
          <div className="faq-block__item">
            <p className="faq-block__question">
              Цели проведения Всероссийской Бестиражной Государственной Лотереи,
              на&nbsp;что идут денежные средства полученные от&nbsp;проведения
              лотерей?
            </p>
            <p className="faq-block__answer">
              Целью проведения Лотерей является поддержка организации
              и&nbsp;проведения XXII Олимпийских зимних игр
              и&nbsp;XI&nbsp;Паралимпийских зимних игр 2014 года в&nbsp;г. Сочи.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">Сроки проведения Лотерей?</p>
            <p className="faq-block__answer">
              От&nbsp;14&nbsp;сентября 2009 года по&nbsp;31&nbsp;декабря
              2016&nbsp;года.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Сколько бестиражных Лотерей проводит Спортлото?
            </p>
            <div className="faq-block__answer">
              <p>Мы&nbsp;проводим десять моментальных бестиражных лотерей:</p>
              <ul>
                <li>&laquo;Спорт без границ&raquo;;</li>
                <li>&laquo;Узоры на&nbsp;льду&raquo;;</li>
                <li>&laquo;Вперёд к&nbsp;победе&raquo;;</li>
                <li>&laquo;Вершины успеха&raquo;;</li>
                <li>&laquo;Поехали&raquo;;</li>
                <li>&laquo;Быстрее,выше,сильнее&raquo;;</li>
                <li>&laquo;Весёлые старты&raquo;;</li>
                <li>&laquo;Спортивный сезон&raquo;;</li>
                <li>&laquo;Праздник спорта&raquo;;</li>
                <li>&laquo;Русские игры&raquo;.</li>
              </ul>
            </div>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">Где можно купить билет?</p>
            <p className="faq-block__answer">
              Лотерейные билеты можно приобрести в традиционной сети
              распространения лотерей.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              На&nbsp;основании чего Оператор проводит Лотереи?
            </p>
            <p className="faq-block__answer">
              На&nbsp;основании Распоряжения Правительства РФ
              &#8470;&nbsp;1318-р от&nbsp;14.09.2009 и&nbsp;приказа Министерства
              Финансов РФ&nbsp;от&nbsp;29&nbsp;декабря 2010&nbsp;г.
              &#8470;&nbsp;195Н &laquo;Об&nbsp;утверждении Условий проведения
              в&nbsp;режиме реального времени 2&nbsp;тиражных всероссийских
              государственных лотерей и&nbsp;в&nbsp;обычном режиме
              10&nbsp;бестиражных всероссийских государственных лотерей
              в&nbsp;поддержку организации и&nbsp;проведения XXII Олимпийских
              зимних игр и&nbsp;XI&nbsp;Паралимпийских зимних игр 2014 года
              в&nbsp;г. Сочи&raquo; утверждённому Министерством
              Финансов&nbsp;РФ.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Может&nbsp;ли гражданин другого государства играть
              в&nbsp;Спортлото?
            </p>
            <p className="faq-block__answer">
              Граждане других государств могут принимать участие в&nbsp;лотереях
              &laquo;Спортлото&raquo;.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Можно&nbsp;ли получить выигрыш на&nbsp;месте приобретения билета?
            </p>
            <p className="faq-block__answer">
              Выигрыш до&nbsp;1000,00 рублей включительно можно получить
              в&nbsp;пункте приобретения Лотерейного билета. В&nbsp;случае
              если&nbsp;Вы участвуете в&nbsp;Лотерее &laquo;Вершины
              успеха&raquo; с&nbsp;выигрышем до&nbsp;4000,00 рублей включительно
              и&nbsp;&laquo;Поехали&raquo; с&nbsp;выигрышем до&nbsp;5000,00
              рублей включительно можно получить в&nbsp;Центральном Телеграфе
              по&nbsp;адресу: Москва, ул. Тверская дом 7.<br />
              Выигрыши свыше указанных сумм можно получить через Оператора
              Лотерей, см. раздел{' '}
              <Link to="/prize_edit">
                &laquo;Проверка и&nbsp;получение выигрыша&raquo;
              </Link>.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Что делать в&nbsp;случае утраты/потери Лотерейного билета?
            </p>
            <p className="faq-block__answer">
              Утерянный Лотерейный билет восстановлению не&nbsp;подлежит. Если
              Вы&nbsp;потеряли свой билет, то&nbsp;любой человек, предъявивший
              утерянный билет сможет получить по&nbsp;нему выигрыш.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Что делать если поврежден защитный слой поля
              &laquo;Не&nbsp;стирать&raquo;?
            </p>
            <div className="faq-block__answer">
              <ul>
                <li>
                  Билеты с&nbsp;повреждённым слоем &laquo;Не&nbsp;стирать&raquo;
                  реализации не&nbsp;подлежат<br />
                  Если при покупке Лотерейного билета распространитель
                  предоставил Вам билет с&nbsp;повреждением контрольного поля
                  &laquo;Не&nbsp;стирать&raquo; Вы&nbsp;имеете право затребовать
                  другой Лотерейный билет, а&nbsp;также сообщить о&nbsp;данном
                  факте нам по&nbsp;телефону, указанному в&nbsp;разделе{' '}
                  <Link to="/contacts">&laquo;Контакты&raquo;</Link>.
                </li>
                <li>
                  Если после удаления защитного слоя с&nbsp;игровых полей слой
                  поля &laquo;Не&nbsp;стирать&raquo; ошибочно удаляет Сотрудник
                  или Участник выигрыш выплачивается в&nbsp;общем порядке.
                </li>
              </ul>
            </div>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Зачем распространитель забирает билет при выплате выигрыша?
            </p>
            <p className="faq-block__answer">
              При выплате Вашего выигрыша, распространитель обязан забрать
              выигрышный билет у&nbsp;Участника для его дальнейшей утилизации
              и&nbsp;целей отчётности.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Что нужно для получения выигрыша через Оператора Лотерей?
            </p>
            <div className="faq-block__answer">
              <p>
                Для получения выигрыша через Спортлото Вам необходимо направить
                в&nbsp;наш адрес комплект документов, включающий:
              </p>
              <ul>
                <li>Лотерейный билет/квитанцию;</li>
                <li>Заявление на&nbsp;выплату выигрыша;</li>
                <li>Банковские реквизиты;</li>
                <li>Копию паспорта</li>
              </ul>
              <p>
                Более подробно с&nbsp;процедурой можно ознакомиться
                в&nbsp;разделе{' '}
                <Link to="/prize_edit">
                  &laquo;Проверка и&nbsp;получение выигрыша&raquo;
                </Link>.
              </p>
            </div>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              В&nbsp;чем заключается экспертиза?
            </p>
            <p className="faq-block__answer">
              Экспертиза заключается в&nbsp;проверке целостности лотерейного
              билета, отсутствия подчисток, нечётких отметок, зачёркивания цифр
              или иной попытки фальсификации лотерейного билета.<br />
              Срок проведения экспертизы&nbsp;&mdash; не&nbsp;более 30&nbsp;дней
              с&nbsp;даты получения оператором и/или уполномоченным лицом
              лотерейного билета на&nbsp;экспертизу. По&nbsp;результатам
              проведения экспертизы, лицом проводившим экспертизу, составляется
              акт, на&nbsp;основании которого ООО &laquo;Спортлото&raquo;
              осуществляет выплату (передачу) выигрыша.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              В&nbsp;какие сроки Спортлото проводит выплату выигрышей?
            </p>
            <p className="faq-block__answer">
              Экспертиза занимает 30&nbsp;календарных дней с&nbsp;момента
              поступления Лотерейного билета в&nbsp;адрес Оператора. Срок
              на&nbsp;выплату выигрышей с&nbsp;момента завершения экспертизы
              составляет 30&nbsp;дней. Общий срок не&nbsp;может превышать
              60&nbsp;календарных дней.<br />
              Для получения более точной информации по&nbsp;выплате Вашего
              выигрыша мы&nbsp;рекомендуем обратиться по&nbsp;номеру указанному
              в&nbsp;разделе <Link to="/contacts">&laquo;Контакты&raquo;</Link>.
            </p>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Нужно&nbsp;ли платить налог с&nbsp;выигрыша и&nbsp;как подавать
              декларацию?
            </p>
            <div className="faq-block__answer">
              <p>
                Мы&nbsp;не&nbsp;являемся налоговыми агентами, поэтому
                ответственность на&nbsp;оплату налога несёт непосредственно
                Участник получивший выигрыш.
              </p>
              <p>
                Мы&nbsp;располагаем только информацией о&nbsp;процентной ставке:
              </p>
              <ul>
                <li>
                  Для граждан&nbsp;РФ налог с&nbsp;выигрыша составляет&nbsp;13%
                  и&nbsp;начисляется с&nbsp;любой суммы выигрыша.
                </li>
                <li>
                  Для нерезидентов&nbsp;РФ ставка налога определяется
                  в&nbsp;соответствии с&nbsp;действующим законодательством,
                  а&nbsp;также международными договорами.
                </li>
              </ul>
            </div>
          </div>
          <div className="faq-block__item">
            <p className="faq-block__question">
              Что происходит с&nbsp;выигрышными билетами с&nbsp;просроченным
              сроком выплат?
            </p>
            <p className="faq-block__answer">
              В&nbsp;течение всего срока проведения ВГБЛ
              (до&nbsp;31&nbsp;декабря 2016&nbsp;г.) выплачиваются выигрыши
              по&nbsp;выигрышным билетам. Затем в&nbsp;соответствии
              со&nbsp;ст.20 п.8&nbsp;ФЗ &laquo;О&nbsp;лотереях&raquo;
              &#8470;&nbsp;138-ФЗ от&nbsp;11.11.2003. Не&nbsp;востребованные
              выигрыши хранятся 5&nbsp;лет и&nbsp;могут быть выданы
              в&nbsp;течение указанного срока по&nbsp;выигрышному билету.
              По&nbsp;истечении указанного срока они зачисляются
              в&nbsp;федеральный бюджет.
            </p>
          </div>
        </div>
        <p className="page__text">
          Не&nbsp;нашли ответ на&nbsp;свой вопрос? Задайте его нам
          и&nbsp;мы&nbsp;обязательно Вам ответим.{' '}
          <a className="page__link" href="mailto:info@sportloto.ru">
            Отправить вопрос в Спортлото
          </a>
        </p>
      </div>
    </div>
  </article>
);

export default Faq;
