import React from 'react';
import { Helmet } from 'react-helmet';
import WinnerSlider from '../../components/WinnerSlider/WinnerSlider';
import BuyLotteryBanner from '../../components/BuyLotteryBanner/BuyLotteryBanner';
import './Winners.css';

const Winners = () => (
  <article className="page page--winners">
    <Helmet>
      <title>Наши победители</title>
      <meta
        name="description"
        content="Истории Победителей. Как выиграть в лотерею? Вероятность выигрыша в лотерею в день своего рождения. Эти и другие вопросы мы задаем нашим победителям, о которых вы можете узнать в данном разделе."
      />
      <meta
        name="keywords"
        content="лотерея на день рождения, вероятность лотерея, как выиграть в лотерею"
      />
    </Helmet>
    <div className="page__tex-block container">
      <h3 className="page__header">Наши победители</h3>
      <p className="page__text">
        В&nbsp;лотереи Спортлото играют и&nbsp;выигрывают миллионы.
        С&nbsp;некоторыми историями победителей можно ознакомиться в&nbsp;этом
        разделе.
      </p>
      <h4 className="page__subheader">Как выиграть в&nbsp;лотерею?</h4>
      <p className="page__text">
        Этот вопрос мы&nbsp;всегда задаём нашим победителям. Многие
        из&nbsp;Участников с&nbsp;крупными выигрышами поделились с&nbsp;нами
        своей историей победы. Некоторые из&nbsp;них участвовали в&nbsp;
        <b>лотерее в&nbsp;день своего рождения</b> или покупали только
        в&nbsp;определенные даты. Многие исследуют <b>вероятность выигрыша</b>{' '}
        в&nbsp;лотерею и&nbsp;покупают сразу несколько билетов. Но, безусловно,
        большинство крупных выигрышей&nbsp;&mdash; это удача и&nbsp;счастливый
        случай. С&nbsp;некоторыми историями победителей лотереи Спортлото можно
        ознакомиться в&nbsp;этом разделе.
      </p>
      <div className="page__slider">
        <WinnerSlider isFilterEnabled={false} />
      </div>
      <div className="page__banner">
        <BuyLotteryBanner />
      </div>
    </div>
  </article>
);

export default Winners;
