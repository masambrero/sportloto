import React from 'react';
import { Helmet } from 'react-helmet';
import BorderBlock from '../../components/BorderBlock/BorderBlock';
import BuyLotteryBanner from '../../components/BuyLotteryBanner/BuyLotteryBanner';
import './Buy.css';

const Buy = () => (
  <article className="page page--buy">
    <Helmet>
      <title>Где купить</title>
      <meta
        name="description"
        content="Где купить лотерейные билеты спортлото, где можно сделать ставку"
      />
      <meta
        name="keywords"
        content="Лотерея спортлото, купить лотерейный билет, играть в лотерею"
      />
    </Helmet>
    <div className="page__info-block page__info-block--shifted container">
      <div className="page__text-block">
        <h3 className="page__header">
          Где купить билеты лотерей &laquo;Спортлото&raquo;?
        </h3>
        <div className="page__promo-list">
          <div className="page__promo-item">
            <BorderBlock
              hasLink
              hasPadding
              hasBottomBorder
              borderType="left"
              linkText="Где купить?"
              linkUrl="https://www.stoloto.ru/map?ad=sportloto"
            >
              <div className="buy-page-block">
                <h3 className="buy-page-block__header">Тиражные лотереи</h3>
                <div className="buy-page-block__banner buy-page-block__banner--lotteries">
                  <BuyLotteryBanner />
                </div>
                <p className="buy-page-block__text">
                  Купить билеты &laquo;Спортлото Матчбол&raquo; вы&nbsp;можете
                  на&nbsp;сайте{' '}
                  <a href="http://www.stoloto.ru">www.stoloto.ru</a>, включая
                  мобильную версию, в&nbsp;мобильных приложениях для iOS
                  и&nbsp;Android, в&nbsp;лотерейных киосках
                  &laquo;Столото&raquo;, букмекерских конторах{' '}
                  <a href="http://www.stoloto.ru/map/search?q=%D0%B1%D0%B0%D0%BB%D1%82%D0%B1%D0%B5%D1%82">
                    &laquo;Балтбет&raquo;
                  </a>, лотерейной сети &laquo;Балтлото&raquo;
                  и&nbsp;по&nbsp;СМС. Тиражи &laquo;Спортлото Матчбол&raquo;
                  проходят ежедневно в&nbsp;20:00 по&nbsp;московскому времени.
                  <a href="https://www.stoloto.ru/live">Прямой эфир.</a>
                  <br />
                  Билеты &laquo;КЕНО-Спортлото&raquo; продаются на&nbsp;сайте{' '}
                  <a href="http://www.stoloto.ru">www.stoloto.ru</a>, включая
                  мобильную версию, в&nbsp;мобильных приложениях для iOS
                  и&nbsp;Android, в&nbsp;лотерейных киосках
                  &laquo;Столото&raquo;, букмекерских конторах{' '}
                  <a href="http://www.stoloto.ru/map/search?q=%D0%B1%D0%B0%D0%BB%D1%82%D0%B1%D0%B5%D1%82">
                    &laquo;Балтбет&raquo;
                  </a>{' '}
                  и&nbsp;лотерейной сети &laquo;Балтлото&raquo;, салонах связи
                  &laquo;Связной&raquo; и&nbsp;по&nbsp;СМС. Тиражи
                  &laquo;КЕНО-Спортлото&raquo; проходят каждые 15&nbsp;минут.
                </p>
              </div>
            </BorderBlock>
          </div>
          <div className="page__promo-item">
            <BorderBlock
              hasBottomBorder
              hasPadding
              hasLink
              linkText="Найти"
              linkUrl="https://www.stoloto.ru/map?ad=sportloto"
            >
              <div className="buy-page-block ">
                <h3 className="buy-page-block__header">Бестиражные лотереи</h3>
                <div className="buy-page-block__banner buy-page-block__banner--instant" />
                <p className="buy-page-block__text">
                  Сыграть в&nbsp;моментальные лотереи вы&nbsp;можете
                  в&nbsp;лотерейных киосках, магазинах &laquo;Связной&raquo;,
                  офисах продаж &laquo;Мегафон&raquo; и&nbsp;отделениях почтовой
                  связи. Билеты также продаются в&nbsp;букмекерских сетях 1XBet,
                  &laquo;Лига ставок&raquo;, &laquo;БукмекерПаб&raquo;,
                  спортбарах &laquo;Торнадо&raquo;, лотерейной сети
                  &laquo;Балт-Лото&raquo;, в&nbsp;киосках &laquo;АиФ&raquo;
                  и&nbsp;&laquo;Роспечать&raquo;. Скорее всего, точки продаж
                  с&nbsp;билетами моментальных лотерей есть рядом с&nbsp;вами.
                </p>
              </div>
            </BorderBlock>
          </div>
        </div>
      </div>
    </div>
  </article>
);

export default Buy;
