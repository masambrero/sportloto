import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import LotteryTab from '../../components/LotteryTab/LotteryTab';
import TicketTab from '../../components/TicketTab/TicketTab';

export default class PrizeEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeLotteryIndex: 0,
    };
  }

  changeActiveLottery = (e) => {
    const newActiveLotteryIndex = parseInt(
      e.target.closest('button').dataset.lottery,
      10,
    );
    return this.setState({ activeLotteryIndex: newActiveLotteryIndex });
  };

  render() {
    const { activeLotteryIndex } = this.state;
    return (
      <article className="page page--prize">
        <Helmet>
          <title>Проверка и получение выигрыша</title>
          <meta name="description" content="Как получить выигрыш в спортлото" />
          <meta
            name="keywords"
            content="Лотерея спортлото, проверить выигрыш, лотерея Матчбол, лотерея КЕНО"
          />
        </Helmet>
        <div className="container">
          <h3 className="page__header">Получение выигрышей</h3>
          <div />
          <div className="page__lotteries page__lotteries--horizontal">
            <LotteryTab
              listDirection="horizontal"
              headerText="Архив тиражей"
              headerLinkUrl="https://www.stoloto.ru/?ad=sportloto"
              activeLotteryIndex={activeLotteryIndex}
              onLotteryClick={this.changeActiveLottery}
            />
          </div>
          <div>
            <TicketTab activeLotteryIndex={activeLotteryIndex} />
          </div>
          {activeLotteryIndex !== 2 && (
            <div className="page__text">
              <p>
                Билеты лотереи &laquo;Спортлото 6&nbsp;из&nbsp;49&raquo;
                вы&nbsp;можете проверить на&nbsp;сайте{' '}
                <a
                  className="page__link"
                  href="https://www.stoloto.ru/6x49/check_bulletin?ad=sportloto_6x49"
                >
                  stoloto.ru.
                </a>
              </p>
            </div>
          )}
        </div>
      </article>
    );
  }
}
