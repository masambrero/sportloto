export const sortUnique = (arr) => [...new Set(arr)];
export const formatNumber = (number) =>
  number &&
  number
    .toString()
    .match(/(\d+?)(?=(\d{3})+(?!\d)|$)/g)
    .join(' ');

const formatDateToTwoDigits = (number) => (number < 10 ? `0${number}` : number);

export function formatDate(currentDate) {
  if (!currentDate) return false;
  const date = new Date(currentDate.replace(/([+-]\d\d)(\d\d)$/, '$1:$2'));
  const months = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря',
  ];

  return `${date.getDate()} ${
    months[date.getMonth()]
  } ${date.getFullYear()} в${'\u00A0'}${formatDateToTwoDigits(
    date.getHours(),
  )}:${formatDateToTwoDigits(date.getMinutes())}`;
}

export function removeNewLinesFromText(text) {
  return text.replace(
    /(<br[\s/]*>)?<div><br[\s/]*>\s+?&nbsp;<\/div>(<br[\s/]*>)?/g,
    '',
  );
}
