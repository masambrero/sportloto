import React, { Component } from 'react';
import './Map.css';

export default class Map extends Component {
  static initMap = () => {
    const LatLng = { lat: 55.714116, lng: 37.718662 };
    const map = new window.google.maps.Map(document.getElementById('map'), {
      center: LatLng,
      zoom: 14,
    });
    const marker = new window.google.maps.Marker({
      position: LatLng,
    });
    marker.setMap(map);
    const content =
      '<div><p>Адрес</p><p>109316, г. Москва,</p><p>Волгоградский проспект, д.&nbsp;43, корп.&nbsp;3</p></div>';
    const infowindow = new window.google.maps.InfoWindow({
      content,
    });
    marker.addListener('click', () => infowindow.open(map, marker));

    window.google.maps.event.trigger(marker, 'click');
  };

  constructor(props) {
    super(props);
    window.initMap = this.constructor.initMap;
  }

  componentDidMount() {
    const fjs = document.getElementsByTagName('script')[0];
    const el = document.createElement('script');
    el.type = 'text/javascript';
    el.src =
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyAPe7qcUtdLl2DE5lsRG0YAh_Frfm4MS90&callback=initMap';
    el.async = true;
    fjs.parentNode.insertBefore(el, fjs);
  }

  render() {
    return <div id="map" />;
  }
}
