import asyncComponent from '../components/AsyncComponent/AsyncComponent';

const Lottery = asyncComponent(() => import('../pages/Lottery/Lottery'));
const About = asyncComponent(() => import('../pages/About/About'));
const Buy = asyncComponent(() => import('../pages/Buy/Buy'));
const PrizeEdit = asyncComponent(() => import('../pages/PrizeEdit/PrizeEdit'));
const News = asyncComponent(() => import('../pages/News/News'));
const ReadNews = asyncComponent(() => import('../pages/ReadNews/ReadNews'));
const Winners = asyncComponent(() => import('../pages/Winners/Winners'));
const Matchball = asyncComponent(() => import('../pages/Matchball/Matchball'));
const Keno = asyncComponent(() => import('../pages/Keno/Keno'));
const TicketTypes = asyncComponent(() =>
  import('../pages/TicketTypes/TicketTypes'),
);
const SportSeason = asyncComponent(() =>
  import('../pages/TicketTypes/Pages/SportSeason'),
);
const FasterHigherStronger = asyncComponent(() =>
  import('../pages/TicketTypes/Pages/FasterHigherStronger'),
);
const ForwardToVictory = asyncComponent(() =>
  import('../pages/TicketTypes/Pages/ForwardToVictory'),
);
const MerryStarts = asyncComponent(() =>
  import('../pages/TicketTypes/Pages/MerryStarts'),
);
const RussianGames = asyncComponent(() =>
  import('../pages/TicketTypes/Pages/RussianGames'),
);
const SportHoliday = asyncComponent(() =>
  import('../pages/TicketTypes/Pages/SportHoliday'),
);
const History = asyncComponent(() => import('../pages/History/History'));
const Faq = asyncComponent(() => import('../pages/Faq/Faq'));
const Payments = asyncComponent(() => import('../pages/Payments/Payments'));
const Press = asyncComponent(() => import('../pages/Press/Press'));
const Partnership = asyncComponent(() =>
  import('../pages/Partnership/Partnership'),
);
const Right = asyncComponent(() => import('../pages/Right/Right'));
const Contacts = asyncComponent(() => import('../pages/Contacts/Contacts'));
const InfoDisclosure = asyncComponent(() =>
  import('../pages/InfoDisclosure/InfoDisclosure'),
);
const NotFound = asyncComponent(() => import('../pages/NotFound/NotFound'));

const routes = [
  {
    path: '/',
    exact: true,
    component: Lottery,
  },
  {
    path: '/about',
    component: About,
    exact: true,
  },
  {
    path: '/buy',
    component: Buy,
    exact: true,
  },
  {
    path: '/prize_edit',
    component: PrizeEdit,
    exact: true,
  },
  {
    path: '/news',
    component: News,
    exact: true,
  },
  {
    path: '/read_news/:id',
    component: ReadNews,
    exact: true,
  },
  {
    path: '/winners',
    component: Winners,
    exact: true,
  },
  {
    path: '/matchball_schedule',
    component: Matchball,
    exact: true,
  },
  {
    path: '/keno_schedule',
    component: Keno,
    exact: true,
  },
  {
    path: '/ticket_types',
    component: TicketTypes,
    exact: true,
  },
  {
    path: '/ticket_types/l_9',
    component: SportSeason,
    exact: true,
  },
  {
    path: '/ticket_types/l_1',
    component: FasterHigherStronger,
    exact: true,
  },
  {
    path: '/ticket_types/l_4',
    component: ForwardToVictory,
    exact: true,
  },
  {
    path: '/ticket_types/l_3',
    component: MerryStarts,
    exact: true,
  },
  {
    path: '/ticket_types/l_6',
    component: SportHoliday,
    exact: true,
  },
  {
    path: '/ticket_types/l_7',
    component: RussianGames,
    exact: true,
  },
  {
    path: '/history',
    component: History,
    exact: true,
  },
  {
    path: '/faq',
    component: Faq,
    exact: true,
  },
  {
    path: '/payments',
    component: Payments,
    exact: true,
  },
  {
    path: '/press',
    component: Press,
    exact: true,
  },
  {
    path: '/partnership',
    component: Partnership,
    exact: true,
  },
  {
    path: '/right',
    component: Right,
    exact: true,
  },
  {
    path: '/contacts',
    component: Contacts,
    exact: true,
  },
  {
    path: '/raskrytie_informatsii',
    component: InfoDisclosure,
    exact: true,
  },
  {
    path: '*',
    component: NotFound,
  },
];

export default routes;
