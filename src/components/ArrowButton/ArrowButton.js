import React from 'react';
import { PropTypes } from 'prop-types';
import SiteLink from '../SiteLink/SiteLink';
import './ArrowButton.css';

const ArrowButton = (props) => {
  const { activeState, linkUrl, isInnerLink, children, onClick } = props;
  const btnClass = `arrow-button ${
    activeState ? `arrow-button--${activeState}` : ''
  }`;
  return linkUrl ? (
    <SiteLink className={btnClass} isInnerLink={isInnerLink} linkUrl={linkUrl}>
      {children}
    </SiteLink>
  ) : (
    <button className={btnClass} onClick={onClick}>
      {children}
    </button>
  );
};

ArrowButton.defaultProps = {
  activeState: false,
  linkUrl: null,
  isInnerLink: false,
  onClick: null,
};

ArrowButton.propTypes = {
  activeState: PropTypes.bool,
  linkUrl: PropTypes.string,
  isInnerLink: PropTypes.bool,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
};

export default ArrowButton;
