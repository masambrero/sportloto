import React from 'react';
import { Link } from 'react-router-dom';
import Slider from '../Slider/Slider';
import ControlButton from '../../components/ControlButton/ControlButton';
import './TicketSlider.css';

const ticketOne = require('../../assets/imgs/tickets/ticket_1.png');
const ticketTwo = require('../../assets/imgs/tickets/ticket_2.png');
const ticketFour = require('../../assets/imgs/tickets/ticket_4.png');
const ticketEight = require('../../assets/imgs/tickets/ticket_8.png');
const ticketNine = require('../../assets/imgs/tickets/ticket_9.png');
const ticketTen = require('../../assets/imgs/tickets/ticket_10.png');

const slides = [
  {
    img: ticketEight,
    title: 'Спортивный сезон',
    price: 25,
    maxPrize: 250000,
    name: 'l_9',
  },
  {
    img: ticketNine,
    title: 'Быстрее, выше, сильнее!',
    price: 100,
    maxPrize: 5000000,
    name: 'l_1',
  },
  {
    img: ticketTen,
    title: 'Вперед к&nbsp;победе',
    price: 50,
    maxPrize: 3000000,
    name: 'l_4',
  },
  {
    img: ticketOne,
    title: 'Праздник спорта',
    price: 25,
    maxPrize: 250000,
    name: 'l_6',
  },
  {
    img: ticketTwo,
    title: 'Веселые старты',
    price: 20,
    maxPrize: 200000,
    name: 'l_3',
  },
  {
    img: ticketFour,
    title: 'Русские игры',
    price: 30,
    maxPrize: 450000,
    name: 'l_7',
  },
];

export default class TicketSlider extends Slider {
  constructor(props) {
    super(props);
    this.slidesPerPage = 3;
    this.increment = 3;
    this.state = {
      activeSlide: 0,
      slidesLength: slides.length,
    };
  }

  render() {
    const { activeSlide, slidesLength } = this.state;
    const canPrev = !(activeSlide === 0);
    const canNext = !(activeSlide === slidesLength - this.slidesPerPage);
    return (
      <div className="ticket-slider">
        {slides.length && (
          <div className="ticket-slider__main-slider">
            <div className="ticket-slider__slide-list">
              {slides.map((i, index) => (
                <div
                  className={`ticket-slider__slide-item ${
                    index === activeSlide
                      ? 'ticket-slider__slide-item--active'
                      : ''
                  } ticket-slide`}
                  style={{
                    transform: `translateX(-${activeSlide * 100}%)`,
                  }}
                  key={index}
                >
                  <Link
                    to={`/ticket_types/${i.name}`}
                    className="ticket-slider__img-wrapper"
                    target="_blank"
                  >
                    <img
                      className="ticket-slide__img"
                      src={i.img}
                      alt="ticket"
                    />
                  </Link>
                  <a
                    href={`/pdf/${i.name}.pdf`}
                    className="ticket-slide__title"
                    dangerouslySetInnerHTML={{ __html: i.title }}
                  />
                  <Link
                    to={`/ticket_types/${i.name}`}
                    className="ticket-slide__btn"
                    target="_blank"
                  >
                    {i.price} рублей
                  </Link>
                  <div className="ticket-slide__max-prize">
                    максимальный выигрыш<span>{i.maxPrize} рублей</span>
                  </div>
                </div>
              ))}
            </div>
            <div className="ticket-slider__controls">
              <ControlButton
                disabled={!canPrev}
                isPrev
                onClick={this.onPrevButtonClick}
              />
              <ControlButton
                disabled={!canNext}
                onClick={this.onNextButtonClick}
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}
