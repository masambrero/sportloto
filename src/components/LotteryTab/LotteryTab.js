import React from 'react';
import { PropTypes } from 'prop-types';
import SiteLink from '../SiteLink/SiteLink';
import LotteryList from '../LotteryList/LotteryList';
import './LotteryTab.css';

const LotteryTab = (props) => {
  const {
    listDirection,
    hasLotteryLink,
    isHeaderLinkInner,
    headerLinkUrl,
    headerText,
    onLotteryClick,
    activeLotteryIndex,
  } = props;
  return (
    <div className="lottery-tab">
      <div
        className={`lottery-tab__header-block ${
          listDirection ? `lottery-tab__header-block--${listDirection}` : ''
        }`}
      >
        <h3 className="lottery-tab__header">
          {headerLinkUrl ? (
            <SiteLink isInnerLink={isHeaderLinkInner} linkUrl={headerLinkUrl}>
              {headerText}
            </SiteLink>
          ) : (
            headerText
          )}
        </h3>
      </div>
      <div className="lottery-tab__list">
        <LotteryList
          listDirection={listDirection}
          hasLink={hasLotteryLink}
          onLotteryClick={onLotteryClick}
          activeLotteryIndex={activeLotteryIndex}
        />
      </div>
    </div>
  );
};

LotteryTab.defaultProps = {
  listDirection: null,
  hasLotteryLink: false,
  isHeaderLinkInner: false,
  headerLinkUrl: null,
  headerText: null,
  activeLotteryIndex: null,
  onLotteryClick: null,
};

LotteryTab.propTypes = {
  listDirection: PropTypes.string,
  hasLotteryLink: PropTypes.bool,
  isHeaderLinkInner: PropTypes.bool,
  headerLinkUrl: PropTypes.string,
  headerText: PropTypes.string,
  activeLotteryIndex: PropTypes.number,
  onLotteryClick: PropTypes.func,
};

export default LotteryTab;
