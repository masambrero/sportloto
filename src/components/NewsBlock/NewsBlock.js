import React from 'react';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import { removeNewLinesFromText } from '../../utils';
import './NewsBlock.css';

const NewsBlock = (props) => (
  <div className="news-block">
    <time className="news-block__date">{props.news.date}</time>
    <Link
      className="news-block__link"
      to={`/read_news/${props.news.id}`}
      dangerouslySetInnerHTML={{
        __html: removeNewLinesFromText(props.news.name),
      }}
    />
    {props.news.announce && (
      <p
        className="news-block__text"
        dangerouslySetInnerHTML={{
          __html: removeNewLinesFromText(props.news.announce),
        }}
      />
    )}
  </div>
);

NewsBlock.propTypes = {
  news: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    announce: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
  }).isRequired,
};

export default NewsBlock;
