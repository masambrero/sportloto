import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Menu from './Menu/Menu';
import asyncComponent from '../../components/AsyncComponent/AsyncComponent';
import './TicketTab.css';

const TicketCheckTab = asyncComponent(() =>
  import('./Views/TicketCheckTab.js'),
);
const ReceiptMethodTab = asyncComponent(() =>
  import('./Views/ReceiptMethodTab.js'),
);
const TicketWonTab = asyncComponent(() => import('./Views/TicketWonTab.js'));
const LetterSendTab = asyncComponent(() => import('./Views/LetterSendTab.js'));
const TicketExpertizeTab = asyncComponent(() =>
  import('./Views/TicketExpertizeTab.js'),
);
const PrizeReceiptTab = asyncComponent(() =>
  import('./Views/PrizeReceiptTab.js'),
);

const lotteryNames = [
  '«Спортлото Матчбол»',
  '«КЕНО-Спортлото»',
  'Моментальные лотереи',
];

const ticketTabsRouteConfig = [
  [TicketCheckTab, ReceiptMethodTab],
  [TicketWonTab, LetterSendTab, TicketExpertizeTab, PrizeReceiptTab],
];

export default class TicketTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeLotteryIndex !== this.props.activeLotteryIndex)
      return this.setState({ progress: 0 });
    return false;
  }

  changeProgress = (progress) => this.setState({ progress });

  render() {
    const { progress } = this.state;
    const { activeLotteryIndex } = this.props;
    const menuType = activeLotteryIndex === 2 ? 1 : 0;
    const CurrentTab = ticketTabsRouteConfig[menuType][progress];
    const lotteryName = lotteryNames[activeLotteryIndex];
    return (
      <div className="ticket-tab">
        <div className="ticket-tab__header">
          <div className="ticket-check__menu">
            <Menu
              menuType={menuType}
              progress={progress}
              changeProgress={this.changeProgress}
            />
          </div>
        </div>
        <div className="ticket-check__info">
          <CurrentTab lotteryName={lotteryName} />
        </div>
      </div>
    );
  }
}

TicketTab.propTypes = {
  activeLotteryIndex: PropTypes.number.isRequired,
};
