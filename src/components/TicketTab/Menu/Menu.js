import React from 'react';
import { PropTypes } from 'prop-types';

const menuConfig = [
  ['проверка билета', 'выбор способа получения выигрыша'],
  [
    'ваш билет выиграл',
    'отправляем письмо',
    'экспертиза билета',
    'получение выигрыша',
  ],
];

const Menu = (props) => {
  const { menuType, progress, changeProgress } = props;
  return (
    <div className="ticket-menu">
      {menuConfig[menuType].map((menuText, index) => (
        <div
          className={`ticket-menu__item ${
            progress === index ? 'ticket-menu__item--active' : ''
          } ${progress > index ? 'ticket-menu__item--passed' : ''}`}
          key={index}
        >
          <button
            className="ticket-menu__btn"
            onClick={() => changeProgress(index)}
          >
            <span className="ticket-menu__number">{index + 1}</span>
            {menuText}
          </button>
        </div>
      ))}
    </div>
  );
};

Menu.propTypes = {
  menuType: PropTypes.number.isRequired,
  progress: PropTypes.number.isRequired,
  changeProgress: PropTypes.func.isRequired,
};

export default Menu;
