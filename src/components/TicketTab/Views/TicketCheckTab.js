import React from 'react';
import { PropTypes } from 'prop-types';

const TicketCheckTab = (props) => {
  const { lotteryName } = props;
  return (
    <div className="ticket-info">
      <h3 className="ticket-info__header">Как проверить билет?</h3>
      <div className="ticket-info__text">
        <ul>
          <li>
            Проверить любые билеты {lotteryName} вы можете на&nbsp;сайте{' '}
            <a href="https://stoloto.ru">stoloto.ru</a>.
          </li>
          <li>
            Выигрыши до&nbsp;50&nbsp;000 рублей наличными можно получить
            в&nbsp;день обращения в&nbsp;
            <a className="page__link" href="https://www.stoloto.ru/faq#tag_g9">
              &nbsp;лотерейном центре &laquo;Столото&raquo;
            </a>.{' '}
            <a
              className="page__link"
              href="https://www.youtube.com/watch?time_continue=2&v=BDYY_QkQGcc"
            >
              Видеомаршрут
            </a>
          </li>
          <li>Телефон горячей линии +7&nbsp;499 270-27-27.</li>
        </ul>
      </div>
    </div>
  );
};

TicketCheckTab.propTypes = {
  lotteryName: PropTypes.string.isRequired,
};

export default TicketCheckTab;
