import React from 'react';

const PrizeReceiptTab = () => (
  <div className="ticket-info">
    <h3 className="ticket-info__header">Получение выигрыша</h3>
    <div className="ticket-info__text">
      <ul>
        <li>
          Выигрыши до&nbsp;1&nbsp;000 рублей включительно выплачиваются
          моментально по&nbsp;месту приобретения Лотерейного билета.
        </li>
        <li>
          Выигрыши до&nbsp;10&nbsp;000 рублей наличными можно получить
          в&nbsp;день обращения в&nbsp;
          <a className="page__link" href="https://www.stoloto.ru/faq#tag_g9">
            &nbsp;лотерейном центре &laquo;Столото&raquo;
          </a>.{' '}
          <a
            className="page__link"
            href="https://www.youtube.com/watch?time_continue=2&v=BDYY_QkQGcc"
          >
            Видеомаршрут
          </a>
        </li>
        <li>
          Выигрыши свыше 1&nbsp;000 рублей выплачиваются ООО
          &laquo;Спортлото&raquo; путём безналичного перевода денежных средств
          на&nbsp;банковский счёт Участника Лотереи.
        </li>
      </ul>
      <p>
        Выплата выигрышей осуществляется не&nbsp;позднее, чем
        в&nbsp;тридцатидневный срок с&nbsp;даты предъявления участником Лотереи
        лотерейного билета ООО &laquo;СПОРТЛОТО&raquo; или уполномоченному
        им&nbsp;лицу для получения выигрыша. В&nbsp;случае необходимости
        проведения экспертизы лотерейного билета срок выплаты выигрышей
        увеличивается на&nbsp;срок проведения экспертизы.
      </p>
    </div>
  </div>
);

export default PrizeReceiptTab;
