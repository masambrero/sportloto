import React from 'react';

const TicketWonTab = () => (
  <div className="ticket-info ticket-info--ticket-won">
    <h3 className="ticket-info__header">Ваш билет выиграл</h3>
    <div className="ticket-info__text">
      <p>
        Билет считается выигравшим, если комбинация символов, расположенных под
        защитным слоем на&nbsp;игровом поле билета, соответствует одной
        из&nbsp;выигрышных комбинаций согласно условиям проведения лотереи.
        Не&nbsp;допускается удаление защитного слоя с&nbsp;контрольного поля
        &laquo;НЕ&nbsp;СТИРАТЬ&raquo;
      </p>
      <h4 className="ticket-info__subheader">Правила лотерей:</h4>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_1.pdf"
          target="_blank"
        >
          Быстрее, Выше, Сильнее!
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_2.pdf"
          target="_blank"
        >
          Вершины успеха
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_3.pdf"
          target="_blank"
        >
          Весёлые старты
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_4.pdf"
          target="_blank"
        >
          Вперёд к победе
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_5.pdf"
          target="_blank"
        >
          Поехали!
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_6.pdf"
          target="_blank"
        >
          Праздник спорта
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_7.pdf"
          target="_blank"
        >
          Русские игры
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_8.pdf"
          target="_blank"
        >
          Спорт без границ
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_9.pdf"
          target="_blank"
        >
          Спортивный сезон
        </a>
      </div>
      <div>
        <a
          className="page__link page__link--pdf"
          href="/pdf/l_10.pdf"
          target="_blank"
        >
          Узоры на льду
        </a>
      </div>
    </div>
  </div>
);

export default TicketWonTab;
