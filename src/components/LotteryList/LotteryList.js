import React from 'react';
import { PropTypes } from 'prop-types';
import SiteLink from '../../components/SiteLink/SiteLink';
import BorderBlock from '../../components/BorderBlock/BorderBlock';
import './LotteryList.css';

const matchballLogo = require('../../assets/imgs/matchball.png');
const kenoLogo = require('../../assets/imgs/keno.png');
const instantLotteriesLogo = require('../../assets/imgs/instant_lottery.png');

const lotteriesInfo = [
  {
    name: 'matchball',
    img: matchballLogo,
    alt: 'matchball logo',
    url: 'http://www.stoloto.ru/5x50/game?ad=sportloto_5x50',
    isInnerLink: false,
  },
  {
    name: 'keno',
    img: kenoLogo,
    alt: 'keno logo',
    url: 'http://www.stoloto.ru/keno/game?ad=sportloto_keno',
    isInnerLink: false,
  },
  {
    name: 'instant',
    img: instantLotteriesLogo,
    alt: 'instant lotteries logo',
    url: '/ticket_types',
    isInnerLink: true,
  },
];

const LotteryList = (props) => {
  const { listDirection, hasLink, activeLotteryIndex, onLotteryClick } = props;
  return (
    <ul
      className={`lottery-list ${
        listDirection ? `lottery-list--${listDirection}` : ''
      }`}
    >
      {lotteriesInfo.map((lottery, index) => (
        <li
          className={`lottery-list__item lottery-list__item--${lottery.name} ${
            activeLotteryIndex === index ? 'lottery-list__item--active' : ''
          }`}
          key={index}
        >
          <BorderBlock
            hasBottomBorder
            hasTopBorder
            hasHover
            borderType="full"
            isActive={activeLotteryIndex === index}
          >
            {hasLink ? (
              <SiteLink isInnerLink={lottery.isInnerLink} linkUrl={lottery.url}>
                <img src={lottery.img} alt={lottery.alt} />
              </SiteLink>
            ) : (
              <button onClick={onLotteryClick} data-lottery={index}>
                <img src={lottery.img} alt={lottery.alt} />
              </button>
            )}
          </BorderBlock>
        </li>
      ))}
    </ul>
  );
};

LotteryList.defaultProps = {
  listDirection: null,
  hasLink: false,
  activeLotteryIndex: null,
  onLotteryClick: null,
};

LotteryList.propTypes = {
  listDirection: PropTypes.string,
  hasLink: PropTypes.bool,
  activeLotteryIndex: PropTypes.number,
  onLotteryClick: PropTypes.func,
};

export default LotteryList;
