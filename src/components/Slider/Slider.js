import { Component } from 'react';

export default class Slider extends Component {
  constructor(props) {
    super(props);
    this.rotate = false;
    this.infinite = false;
    this.increment = 1;
  }

  componentDidMount() {
    if (this.rotate) this.intervalId = setInterval(this.autoRotateSlides, 4000);
  }

  componentWillUnmount() {
    if (this.intervalId) clearInterval(this.intervalId);
  }

  onNextButtonClick = () => {
    this.changeSlide(true);
  };

  onPrevButtonClick = () => {
    this.changeSlide(false);
  };

  getNextSlideIndex = (slideIndex) => {
    const { slidesLength } = this.state;
    if (slideIndex >= slidesLength - this.increment) {
      return this.infinite ? 0 : slideIndex;
    }
    return slideIndex + this.increment;
  };

  getPrevSlideIndex = (slideIndex) => {
    const { slidesLength } = this.state;
    if (slideIndex <= this.increment - 1) {
      return this.infinite ? slidesLength - this.increment : slideIndex;
    }
    return slideIndex - this.increment;
  };

  changeSlide = (isIncreasing, options = {}) => {
    if (this.rotate) {
      clearInterval(this.intervalId);
      this.intervalId = setInterval(this.autoRotateSlides, 8000);
    }
    if (options.nextSlide) {
      return this.setState({ activeSlide: options.nextSlide }); //eslint-disable-line
    }

    return this.setState((prevState) => {
      if (isIncreasing) {
        return {
          activeSlide: this.getNextSlideIndex(prevState.activeSlide),
        };
      }
      return {
        activeSlide: this.getPrevSlideIndex(prevState.activeSlide),
      };
    });
  };

  autoRotateSlides = () =>
    this.changeSlide(null, this.getNextSlideIndex(this.state));
}
