import React from 'react';
import { Route, Switch, NavLink, withRouter } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import 'babel-polyfill';
import Footer from './Footer/Footer';
import routes from './routes';

const SportlotoImg = require('../assets/imgs/logo.gif');

const App = withRouter(({ location }) => (
  <div className="app">
    <nav className="site-nav">
      <div className="site-nav__header">
        <NavLink to="/" className="site-nav__logo">
          <img
            src={SportlotoImg}
            alt="Всероссийские государственные лотереи Спортлото"
            title="Спортлото"
          />
        </NavLink>
        <div className="site-nav__phone">
          <h6>
            +7 499 270-27-27
            <span>Горячая линия</span>
          </h6>
        </div>
      </div>
      <div className="site-nav__menu-wrapper">
        <ul className="site-nav__menu-list">
          <li className="site-nav__menu-item">
            <NavLink
              to="/"
              className="site-nav__menu-link"
              activeClassName="site-nav__menu-link--active"
              exact
            >
              <span>Лотереи</span>
            </NavLink>
          </li>
          <li className="site-nav__menu-item">
            <NavLink
              to="/about"
              className="site-nav__menu-link"
              activeClassName="site-nav__menu-link--active"
            >
              <span>О компании</span>
            </NavLink>
          </li>
          <li className="site-nav__menu-item">
            <NavLink
              to="/buy"
              className="site-nav__menu-link"
              activeClassName="site-nav__menu-link--active"
            >
              <span>Где купить</span>
            </NavLink>
          </li>
          <li className="site-nav__menu-item">
            <NavLink
              to="/prize_edit"
              className="site-nav__menu-link"
              activeClassName="site-nav__menu-link--active"
            >
              <span>Получение выигрышей</span>
            </NavLink>
          </li>
          <li className="site-nav__menu-item">
            <NavLink
              to="/news"
              className="site-nav__menu-link"
              activeClassName="site-nav__menu-link--active"
            >
              <span>Новости</span>
            </NavLink>
          </li>
          <li className="site-nav__menu-item">
            <NavLink
              to="/winners"
              className="site-nav__menu-link"
              activeClassName="site-nav__menu-link--active"
            >
              <span>Победители</span>
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
    <TransitionGroup component="main">
      <CSSTransition key={location.key} timeout={300} classNames="fade">
        <div className="content">
          <Switch location={location}>
            {routes.map((route, i) => <Route key={i} {...route} />)}
          </Switch>
        </div>
      </CSSTransition>
    </TransitionGroup>
    <Footer />
  </div>
));

export default App;
