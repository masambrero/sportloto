import React from 'react';
import { Link } from 'react-router-dom';
import SocialTab from '../SocialTab/SocialTab';
import './Footer.css';

const Footer = () => (
  <footer className="site-footer">
    <nav className="site-footer__nav">
      <ul className="site-footer__list">
        <li className="site-footer__item">
          <Link to="/history">
            <span>История</span>
          </Link>
        </li>
        <li className="site-footer__item">
          <Link to="/faq">
            <span>Вопросы и ответы</span>
          </Link>
        </li>
        <li className="site-footer__item">
          <Link to="/payments">
            <span>Выигрыши</span>
          </Link>
        </li>
        <li className="site-footer__item">
          <Link to="/press">
            <span>Пресс-центр</span>
          </Link>
        </li>
        <li className="site-footer__item">
          <Link to="/partnership">
            <span>Распространителям</span>
          </Link>
        </li>
      </ul>
    </nav>
    <div className="site-footer__social-tab">
      <SocialTab />
    </div>
    <div className="site-footer__contacts footer-contacts">
      <ul className="footer-contacts__info-list">
        <li className="footer-contacts__info-item footer-contacts__info-item--copyright">
          &copy; Все права защищены &laquo;Спортлото&raquo;
        </li>
        <li className="footer-contacts__info-item">
          <Link to="/right/">Правовая информация</Link>
        </li>
        <li className="footer-contacts__info-item">
          <Link to="/contacts/">Контакты</Link>
        </li>
      </ul>
      <h6 className="footer-contacts__phone">
        +7 499 270-27-27<span>Горячая линия</span>
      </h6>
    </div>
  </footer>
);

export default Footer;
