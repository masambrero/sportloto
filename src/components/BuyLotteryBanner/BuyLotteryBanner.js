import React from 'react';
import './BuyLotteryBanner.css';

const mmatchballLogo = require('../../assets/imgs/matchball.png');
const kenoLogo = require('../../assets/imgs/keno.png');

const BuyLotteryBanner = () => (
  <div className="buy-lottery-banner">
    <div className="buy-lottery-banner__item">
      <img src={mmatchballLogo} alt="5x50 lottery logo" />
      <a
        className="buy-lottery-banner__btn btn"
        href="https://www.stoloto.ru/5x50/game?ad=sportloto_5x50"
      >
        Купить билет
      </a>
    </div>
    <div className="buy-lottery-banner__item">
      <img src={kenoLogo} alt="keno lottery logo" />
      <a
        className="buy-lottery-banner__btn btn"
        href="https://www.stoloto.ru/quickgames/keno?ad=sportloto_keno"
      >
        Купить билет
      </a>
    </div>
  </div>
);

export default BuyLotteryBanner;
