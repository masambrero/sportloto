import React, { Component } from 'react';
import './SocialTab.css';

export default class SocialTab extends Component {
  static loadFacebook() {
    window.fbAsyncInit = function() {
      window.FB.init({
        appId: 'stoloto.ru',
        autoLogAppEvents: true,
        xfbml: true,
        version: 'v2.11',
      });
    };

    (function(d, s, id) {
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      const js = d.createElement(s);
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
  }

  static loadVk() {
    window.vkAsyncInit = function() {
      window.VK.Widgets.Group('vk_groups', { mode: 3 }, 24631493);
    };

    setTimeout(function() {
      const fjs = document.getElementsByTagName('script')[0];
      const el = document.createElement('script');
      el.type = 'text/javascript';
      el.src = 'https://vk.com/js/api/openapi.js?151';
      el.async = true;
      fjs.parentNode.insertBefore(el, fjs);
    }, 0);
  }

  static loadTwitter() {
    window.twttr = (function(d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://platform.twitter.com/widgets.js';
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };

      return t;
    })(document, 'script', 'twitter-wjs');
  }

  componentDidMount() {
    this.constructor.loadFacebook();
    this.constructor.loadVk();
    this.constructor.loadTwitter();
  }

  render() {
    return (
      <div className="social-tab">
        <div
          className="social-tab__item social-tab__item--fb fb-page"
          data-href="https://www.facebook.com/stoloto.ru"
          data-width="238"
          data-show-faces="true"
          data-stream="false"
          data-header="true"
        />
        <div id="vk_groups" className="social-tab__item social-tab__item--vk" />
        <div className="social-tab__item social-tab__item--tw">
          <a
            className="twitter-share-button"
            href="https://twitter.com/intent/tweet?text=https://www.sportloto.ru/"
            data-size="large"
          >
            Tweet
          </a>
        </div>
      </div>
    );
  }
}
