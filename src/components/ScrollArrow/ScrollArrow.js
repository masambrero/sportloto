import React, { Component } from 'react';
import SmoothScroll from 'smooth-scroll';
import './ScrollArrow.css';

const requestAnimationFrame =
  window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  window.oRequestAnimationFrame;

export default class ScrollArrow extends Component {
  constructor(props) {
    super(props);
    this.scroll = new SmoothScroll();
    this.state = {
      isScrollShown: false,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.smoothScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.smoothScroll);
  }

  smoothScroll = () => {
    if (!requestAnimationFrame) return this.showScrollIfNeeded();
    return requestAnimationFrame(this.showScrollIfNeeded);
  };

  showScrollIfNeeded = () => {
    const { isScrollShown } = this.state;
    return window.scrollY >= window.outerHeight
      ? this.setState({ isScrollShown: true })
      : isScrollShown && this.setState({ isScrollShown: false });
  };

  scrollTop = () => {
    if (window.scrollY === 0) return false;
    return this.scroll.animateScroll(0);
  };

  render() {
    const { isScrollShown } = this.state;
    return isScrollShown ? (
      <button className="scroll-arrow" onClick={this.scrollTop} />
    ) : null;
  }
}
