import React from 'react';
import { PropTypes } from 'prop-types';
import ArrowButton from '../ArrowButton/ArrowButton';
import './BorderBlock.css';

const BorderBlock = (props) => {
  const {
    linkText,
    linkUrl,
    hasLink,
    isInnerLink,
    hasPadding,
    hasTopBorder,
    hasBottomBorder,
    hasHover,
    isActive,
    children,
    borderType,
  } = props;
  return (
    <div
      className={`border-block ${hasHover ? 'border-block--hasHover' : ''} ${
        isActive ? 'border-block--active' : ''
      }`}
    >
      {hasTopBorder && (
        <div
          className={`border-block__border border-block__border--top ${
            borderType ? `border-block__border--top-${borderType}` : ''
          }`}
        />
      )}
      {children}
      {hasLink && (
        <div
          className={`border-block__btn ${
            hasPadding ? 'border-block__btn--padded' : ''
          }`}
        >
          <ArrowButton linkUrl={linkUrl} isInnerLink={isInnerLink}>
            {linkText}
          </ArrowButton>
        </div>
      )}
      {hasBottomBorder && (
        <div
          className={`border-block__border border-block__border--bottom ${
            borderType ? `border-block__border--bottom-${borderType}` : ''
          }`}
        />
      )}
    </div>
  );
};

BorderBlock.defaultProps = {
  linkText: null,
  linkUrl: null,
  hasLink: false,
  isInnerLink: false,
  hasPadding: false,
  hasTopBorder: false,
  hasBottomBorder: false,
  hasHover: false,
  isActive: false,
  borderType: 'full',
};

BorderBlock.propTypes = {
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
  hasLink: PropTypes.bool,
  isInnerLink: PropTypes.bool,
  hasPadding: PropTypes.bool,
  hasTopBorder: PropTypes.bool,
  hasBottomBorder: PropTypes.bool,
  hasHover: PropTypes.bool,
  isActive: PropTypes.bool,
  children: PropTypes.node.isRequired,
  borderType: PropTypes.string,
};

export default BorderBlock;
