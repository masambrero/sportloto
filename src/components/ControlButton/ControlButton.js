import React from 'react';
import { PropTypes } from 'prop-types';
import './ControlButton.css';

const ControlButton = ({ onClick, classNames, children, isPrev, disabled }) => (
  <button
    onClick={onClick}
    className={`control-btn ${isPrev ? 'control-btn--prev' : ''} ${[
      ...classNames,
    ]}`}
    disabled={disabled}
  >
    {children}
  </button>
);

ControlButton.defaultProps = {
  classNames: [],
  children: null,
  isPrev: false,
  disabled: false,
};

ControlButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  classNames: PropTypes.arrayOf(PropTypes.string),
  isPrev: PropTypes.bool,
  children: PropTypes.node,
  disabled: PropTypes.bool,
};

export default ControlButton;
