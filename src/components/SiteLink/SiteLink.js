import React from 'react';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';

const SiteLink = (props) => {
  const { isInnerLink, className, linkUrl, children } = props;
  return isInnerLink ? (
    <Link to={linkUrl} className={className}>
      {children}
    </Link>
  ) : (
    <a href={linkUrl} className={className}>
      {children}
    </a>
  );
};

SiteLink.defaultProps = {
  isInnerLink: false,
  className: null,
};

SiteLink.propTypes = {
  isInnerLink: PropTypes.bool,
  className: PropTypes.string,
  linkUrl: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default SiteLink;
