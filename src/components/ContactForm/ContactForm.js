import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';
import './ContactForm.css';

export default class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        fio: 'Иван Иванович Иванов',
        organization: '',
        position: '',
        phone: '',
        email: '',
        about: '',
        tradePointsCount: '',
        region: '',
        netAccess: 0,
      },
    };
  }

  changeField = (e) => {
    e.persist();
    this.setState((prevState) => ({
      formData: { ...prevState.formData, [e.target.name]: e.target.value },
    }));
  };

  sendForm = (e) => {
    e.preventDefault();
    this.setState({ isFecthing: true });
    fetch('/partnershipform', {
      method: 'post',
      data: this.state.formData,
    })
      .then((response) => {
        if (response.status !== 200) {
          console.log(`Error sending form! ${response.status}`);
        }
        return response.json();
      })
      .then((data) => {
        this.setState({ isFecthing: false, result: data.result });
      })
      .catch((error) => {
        console.log(error);
        this.setState({ isFecthing: false });
      });
  };

  render() {
    const {
      formData: {
        fio,
        organization,
        position,
        phone,
        email,
        about,
        tradePointsCount,
        region,
        netAccess,
      },
      isFecthing,
      result,
    } = this.state;
    return (
      <form
        className={`contact-form ${isFecthing ? 'contact-form--faded' : ''}`}
        onSubmit={this.sendForm}
      >
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            Ф.И.О. контактного лица
            <input
              className="contact-form__input"
              value={fio}
              name="fio"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            Наименование организации
            <input
              className="contact-form__input"
              value={organization}
              name="organization"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            Должность
            <input
              className="contact-form__input"
              value={position}
              name="position"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            Телефон для связи
            <input
              className="contact-form__input"
              value={phone}
              name="phone"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            Электронная почта
            <input
              className="contact-form__input"
              value={email}
              name="email"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            На&nbsp;чем специализируется организация
            <textarea
              className="contact-form__input"
              rows="7"
              value={about}
              name="about"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            Количество торговых точек
            <input
              className="contact-form__input"
              value={tradePointsCount}
              name="tradePointsCount"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            В&nbsp;каком регионе планируете распространять билеты
            <input
              className="contact-form__input"
              value={region}
              name="region"
              onChange={this.changeField}
            />
          </label>
        </p>
        <p className="contact-form__input-block">
          <label className="contact-form__label">
            Наличие доступа в&nbsp;торговых точках к&nbsp;сети Интернет
            <select
              className="contact-form__input"
              value={netAccess}
              name="netAccess"
              onChange={this.changeField}
            >
              <option value="1">Да</option>
              <option value="2">Да, но&nbsp;не&nbsp;у&nbsp;всех</option>
              <option value="3">Нет</option>
            </select>
          </label>
        </p>
        <p className="contact-form__input-block contact-form__input-block--btn-block">
          <button className="contact-form__submit-btn btn">Отправить</button>
        </p>
        {result && <div>Hey i am result!</div>}
      </form>
    );
  }
}
