import React from 'react';
import { Link } from 'react-router-dom';
import Slider from '../../components/Slider/Slider';
import './LotterySlider.css';

const matchballLogo = require('../../assets/imgs/matchball.png');
const kenoLogo = require('../../assets/imgs/keno.png');
const instantLotteriesLogo = require('../../assets/imgs/instant_lottery.png');
const fiveMillionImg = require('../../assets/imgs/5_million.png');

const slides = [
  <div className="lottery-slide-item">
    <h2 className="lottery-slide-item__header">Более миллиарда рублей</h2>
    <h3 className="lottery-slide-item__subheader">
      Выплачено по&nbsp;выигрышам лотерей, оператором которых является ООО
      &laquo;Спортлото&raquo;.
    </h3>
    <p className="lottery-slide-item__text">
      <Link to="/payments">
        Каждую секунду продается 3&nbsp;лотерейных билета.
      </Link>
    </p>
  </div>,
  <div className="lottery-slide-item">
    <h2 className="lottery-slide-item__header">
      Новая лотерея от&nbsp;&laquo;Спортлото&raquo;
    </h2>
    <h3 className="lottery-slide-item__subheader">
      &laquo;Спортлото Матчбол&raquo;&nbsp;&mdash; это современная версия
      классического &laquo;Спортлото&raquo;. Это верность традициям, больше
      возможностей выиграть и&nbsp;выше призы!
    </h3>
    <p className="lottery-slide-item__text">
      <Link to="https://www.stoloto.ru/5x50/game?ad=sportloto_5x50">
        Выберите 5&nbsp;чисел от&nbsp;1&nbsp;до&nbsp;50&nbsp;в поле
        1&nbsp;и&nbsp;одно число от&nbsp;1&nbsp;до&nbsp;11&nbsp;в поле 2.
        Выигрывают комбинации, в&nbsp;которых угадано от&nbsp;двух чисел
        в&nbsp;поле 1&nbsp;или одно число, он&nbsp;же бонусный шар, в&nbsp;поле
        2. Если вы&nbsp;угадали 5&nbsp;чисел в&nbsp;первом поле и&nbsp;бонусный
        шар&nbsp;&mdash; получаете суперприз. Тиражи &laquo;Спортлото
        Матчбол&raquo; проходят каждый день в&nbsp;20:00 по&nbsp;московскому
        времени.
      </Link>
    </p>
  </div>,
  <div className="lottery-slide-item">
    <h2 className="lottery-slide-item__header">Миллион каждый день!</h2>
    <h3 className="lottery-slide-item__subheader">
      Тиражная лотерея со&nbsp;стоимостью билета от&nbsp;60&nbsp;рублей
      и&nbsp;с&nbsp;суперпризом от&nbsp;10&nbsp;миллионов рублей.
    </h3>
    <p className="lottery-slide-item__text">
      <Link to="https://www.stoloto.ru/quickgames/keno?ad=sportloto_keno">
        Нужно выбрать от&nbsp;1&nbsp;до&nbsp;10&nbsp;номеров
        из&nbsp;80&nbsp;возможных. Вы&nbsp;сами выбираете количество чисел. Чем
        больше чисел вы&nbsp;выбрали и&nbsp;угадали, тем больше ваш выигрыш.
        Минимальный гарантированный суперприз составляет
        10&nbsp;000&nbsp;000&nbsp;рублей. Тиражи проводятся каждые
        15&nbsp;минут.
      </Link>
    </p>
  </div>,
  <div className="lottery-slide-item">
    <h2 className="lottery-slide-item__header">
      Выиграйте до&nbsp;5&nbsp;миллионов рублей!
    </h2>
    <h3 className="lottery-slide-item__subheader">
      Спрашивайте в&nbsp;отделениях Почты России, магазинах
      &laquo;Связной&raquo;, пунктах продаж лотерей и&nbsp;лотерейном центре
      &laquo;Столото&raquo;
    </h3>
    <p className="lottery-slide-item__text">
      <Link to="/ticket_types">Билеты от&nbsp;20&nbsp;до&nbsp;100 рублей</Link>
    </p>
  </div>,
];

export default class LotterySlider extends Slider {
  constructor(props) {
    super(props);
    this.rotate = true;
    this.infinite = true;
    this.state = {
      activeSlide: 0,
      slidesLength: slides.length || 0,
    };
  }

  render() {
    const { activeSlide } = this.state;
    return (
      <div className="lottery-slider">
        <div className="lottery-slider__lottery-list">
          <div
            className="lottery-slider__lottery-item lottery-item"
            onMouseEnter={() => this.changeSlide(null, { nextSlide: 1 })}
          >
            <div className="lottery-item__wrapper">
              <img
                className="lottery-item__logo"
                src={matchballLogo}
                alt="matchball lottery logo"
              />
              <a
                className="lottery-item__btn btn btn--matchball"
                href="https://www.stoloto.ru/5x50/game?ad=sportloto_5x50"
              >
                Купить билет
              </a>
              <Link
                className="lottery-item__btn btn btn--matchball"
                to="/matchball_schedule"
              >
                Информация
              </Link>
            </div>
          </div>
          <div className="lottery-slider__vertical-divider" />
          <div
            className="lottery-slider__lottery-item lottery-item"
            onMouseEnter={() => this.changeSlide(null, { nextSlide: 2 })}
          >
            <div className="lottery-item__wrapper">
              <img
                className="lottery-item__logo"
                src={kenoLogo}
                alt="keno lottery logo"
              />
              <a
                className="lottery-item__btn btn btn--keno"
                href="https://www.stoloto.ru/quickgames/keno?ad=sportloto_keno"
              >
                Купить билет
              </a>
              <Link
                className="lottery-item__btn btn btn--keno"
                to="/keno_schedule"
              >
                Информация
              </Link>
            </div>
          </div>
          <div className="lottery-slider__vertical-divider" />
          <div
            className="lottery-slider__lottery-item lottery-item"
            onMouseEnter={() => this.changeSlide(null, { nextSlide: 3 })}
          >
            <div className="lottery-item__wrapper">
              <img
                className="lottery-item__logo"
                src={instantLotteriesLogo}
                alt="instant lottery logo"
              />
              <Link
                className="lottery-item__btn btn btn--instant"
                to="/ticket_types"
              >
                Подробнее
              </Link>
              <h6 className="lottery-item__info">Максимальный выигрыш</h6>
              <img
                className="lottery-item__prize"
                src={fiveMillionImg}
                alt="max prize 5 millions"
              />
            </div>
          </div>
        </div>
        <div className="lottery-slider__promo-tab">
          {slides.map((i, index) => (
            <div
              key={index}
              className={`lottery-slider__slide slide-info ${
                index === activeSlide ? 'slide-info--active' : ''
              } slide-info--${index}`}
            >
              <div className="slide-info__wrapper">
                <div className="slide-info__inner">{i}</div>
                <div className="slide-info__controls">
                  <button
                    className="slide-info__control-button slide-info__control-button--prev"
                    onClick={this.onPrevButtonClick}
                  />
                  <button
                    className="slide-info__control-button slide-info__control-button--next"
                    onClick={this.onNextButtonClick}
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
