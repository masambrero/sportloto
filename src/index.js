import React from 'react';
import { render } from 'react-snapshot';
import { BrowserRouter } from 'react-router-dom';
import 'normalize.css';
import './style.css';
import App from './components/App';
import ScrollTop from './components/ScrollTop/ScrollTop';

render(
  <BrowserRouter>
    <ScrollTop>
      <App />
    </ScrollTop>
  </BrowserRouter>,
  document.getElementById('root'),
);
